## Start project
Install libraries with:
`yarn`
then run
`yarn start`

## Run eslint
`yarn lint`

## Run tests
`yarn run test`

## To add a package
`yarn add [--dev] [package_name]`
