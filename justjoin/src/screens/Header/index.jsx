import './style.sass';

import * as actions from '../../store/auth/actions';

import { Button, Icon } from '@material-ui/core';

import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import React from 'react';
import { connect } from 'react-redux';
import settings from '../../config';
import { withRouter } from 'react-router';

class Header extends React.PureComponent {
  render() {
    const { token, logout, history, userData } = this.props;

    return (
      <nav>
        {token.length > 0 && (
          <div className="loggedAs">
            <Icon>person</Icon>
            Logged in as {userData.email}
          </div>
        )}
        <div className="pageTitle">
          {/* eslint-disable-next-line */}
          <h1 onClick={() => history.push('/')} className="center">
            NoFluffJobs
          </h1>
        </div>
        <div className="center">
          <Button component={Link} to={settings.URLS.ROOT}>
            {' '}
            Browse jobs{' '}
          </Button>
          <Button component={Link} to={settings.URLS.ADD_JOB}>
            {' '}
            Add a job{' '}
          </Button>
          <Button component={Link} to={settings.URLS.COMPANIES}>
            Companies
          </Button>
          <Button component={Link} to={settings.URLS.DASHBOARD}>
            {' '}
            Dashboard{' '}
          </Button>
          {token.length > 0 ? (
            <Button
              onClick={() =>
                logout().then(() => {
                  history.push(settings.URLS.ROOT);
                })
              }>
              Log out
            </Button>
          ) : (
            <span>
              <Button component={Link} to={settings.URLS.LOGIN}>
                Log in
              </Button>
              <Button className="btn" component={Link} to={settings.URLS.REGISTER}>
                Register
              </Button>
            </span>
          )}
        </div>
      </nav>
    );
  }
}

const mapStateToProps = state => ({ token: state.auth.token, userData: state.auth.data });

Header.propTypes = {
  token: PropTypes.string.isRequired,
  logout: PropTypes.func.isRequired,
  history: PropTypes.shape({}).isRequired,
  userData: PropTypes.shape({}),
};

export default withRouter(
  connect(
    mapStateToProps,
    {
      logout: actions.logout,
    },
  )(Header),
);
