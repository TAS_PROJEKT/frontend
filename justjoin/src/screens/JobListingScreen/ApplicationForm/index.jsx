import './style.sass';

import FileLoader from '../../../common/FileLoader';
import PropTypes from 'prop-types';
import React from 'react';
import SETTINGS from '../../../config';
import { TextField } from '@material-ui/core';

class ApplicationForm extends React.PureComponent {
  handleFileUpload = file => {
    const { onCvUpload, job } = this.props;
    if (job) {
      onCvUpload(file, job);
    }
  };

  render() {
    const { onChange, name, comment, uploaded, uploading, fileName, error } = this.props;

    return (
      <div>
        <div className="row">
          <TextField
            required
            fullWidth
            placeholder="Name"
            onChange={evt => onChange('name', evt.target.value)}
            value={name}
          />
        </div>{' '}
        <div className="row">
          <TextField
            required
            fullWidth
            multiline
            placeholder="Comment"
            onChange={evt => onChange('comment', evt.target.value)}
            value={comment}
          />
        </div>
        <div className="row">
          <FileLoader
            extensions={SETTINGS.CV_EXTENSIONS}
            error={error}
            fileName={fileName}
            uploading={uploading}
            uploaded={uploaded}
            onFileUpload={this.handleFileUpload}
            label="Upload CV"
          />
        </div>
      </div>
    );
  }
}

ApplicationForm.propTypes = {
  job: PropTypes.number,
  comment: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
  onCvUpload: PropTypes.func.isRequired,
  uploading: PropTypes.bool,
  uploaded: PropTypes.bool,
  fileName: PropTypes.string.isRequired,
  error: PropTypes.string.isRequired,
};

ApplicationForm.defaultProps = {
  uploading: false,
  uploaded: false,
  job: null,
};

export default ApplicationForm;
