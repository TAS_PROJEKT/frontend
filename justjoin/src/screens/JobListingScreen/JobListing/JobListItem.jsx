import './style.sass';

import Paper from '@material-ui/core/Paper';
import PropTypes from 'prop-types';
import React from 'react';
import Tool from '../Tool';
import { jobDateToLabel } from '../../../helpers';

const JobListItem = ({
  title,
  onClick,
  tools,
  salaryHigh,
  salaryLow,
  currency,
  companyName,
  companyLogo,
  dateCreated,
}) => (
  <Paper className="container" onClick={onClick}>
    <div className="leftSide">
      <div className="logoContainer">
        <img className="logo" src={companyLogo} alt="" />
      </div>
      <span className="info">
        <div className="title">{title}</div>
        <div className="companyName">{companyName}</div>
      </span>
    </div>
    <div className="rightSide">
      <span className="salary">
        {salaryLow} - {salaryHigh} {currency}
        <span className="dateBadge">{jobDateToLabel(dateCreated)}</span>
      </span>
      {tools.length > 0 && (
        <span>
          {tools.map(toolObject => (
            <Tool key={toolObject.id} tool={toolObject.tool} />
          ))}
        </span>
      )}
    </div>
  </Paper>
);

JobListItem.propTypes = {
  title: PropTypes.string.isRequired,
  salaryHigh: PropTypes.string.isRequired,
  currency: PropTypes.string.isRequired,
  salaryLow: PropTypes.string.isRequired,
  onClick: PropTypes.func,
  tools: PropTypes.arrayOf(PropTypes.shape({})),
  companyName: PropTypes.string.isRequired,
  companyLogo: PropTypes.string.isRequired,
  dateCreated: PropTypes.string.isRequired,
};

export default JobListItem;
