import JobListItem from './JobListItem';
import PropTypes from 'prop-types';
import React from 'react';

const JobListing = ({ data, onClick, hovered }) =>
  Object.keys(data).map(key => (
    <JobListItem
      onClick={() => onClick(key)}
      key={key}
      salaryLow={data[key].salaryLow}
      salaryHigh={data[key].salaryHigh}
      contactEmail={data[key].salaryHigh}
      tools={data[key].tools}
      currency={data[key].salaryHighCurrency}
      title={data[key].title}
      dateCreated={data[key].created}
      companyName={data[key].company.company_name}
      companyLogo={data[key].company.logo}
      hovered={hovered.includes(key)}
    />
  ));

JobListing.propTypes = {
  data: PropTypes.shape({}),
  onClick: PropTypes.func,
  hovered: PropTypes.arrayOf(PropTypes.string),
};

export default JobListing;
