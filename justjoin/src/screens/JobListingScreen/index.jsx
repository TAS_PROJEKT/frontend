import './style.sass';

import { Button, Dialog, DialogActions, DialogContent, DialogTitle } from '@material-ui/core';
import React, { PureComponent } from 'react';
import {
  createApplication,
  deleteApplication,
  updateApplication,
  uploadCv,
} from '../../store/applications/actions';

import ApplicationForm from './ApplicationForm';
import JobDetail from './JobDetail';
import JobListing from './JobListing';
import Map from '../../common/Map';
import PermissionDialog from '../../common/PermissionDialog';
import PropTypes from 'prop-types';
import SETTINGS from '../../config';
import Toolbar from './Toolbar';
import { connect } from 'react-redux';
import { fetchJobs } from '../../store/jobs/actions';
import { fetchUserData } from '../../store/auth/actions';
import { withRouter } from 'react-router-dom';

class JobListingScreen extends PureComponent {
  modes = {
    list: 'list',
    detail: 'detail',
  };

  constructor(props) {
    super(props);
    this.state = {
      selectedJob: null,
      mode: this.modes.list,
      hovered: [],
      applyModalOpen: false,
      permissionDialogOpen: false,
      name: '',
      comment: '',
    };
  }

  componentDidMount() {
    const { fetchJobsAction } = this.props;
    fetchJobsAction();
    fetchUserData(); // do wywalenia, tymczasowo dla wygody
  }

  handleJobClick = id => {
    this.setState({ selectedJob: id, mode: this.modes.detail });
  };

  renderJobListing = () => {
    const { jobs, match } = this.props;
    const { hovered } = this.state;
    return (
      <JobListing
        tool={match.params.tool}
        onClick={this.handleJobClick}
        data={jobs}
        hovered={hovered}
      />
    );
  };

  handleBackButtonClick = () => {
    this.setState({ mode: this.modes.list, selectedJob: null });
  };

  handleMarkerHover = ids => {
    this.setState({ hovered: [ids] });
  };

  handleApplyClick = () => {
    const { createApplicationAction } = this.props;
    const { selectedJob } = this.state;

    createApplicationAction(selectedJob).then(status => {
      if (status === 403) {
        this.setState({ permissionDialogOpen: true });
      } else {
        this.setState({ applyModalOpen: true });
      }
    });
  };

  handleCancel = () => {
    const { deleteApplicationAction } = this.props;

    this.closeDialog();
    deleteApplicationAction();
  };

  handleSend = () => {
    const { updateApplicationAction } = this.props;
    const { name, comment, selectedJob } = this.state;

    updateApplicationAction({ name, comment, job: selectedJob });
    this.closeDialog();
  };

  handleChange = (name, value) => {
    this.setState({ [name]: value });
  };

  closeDialog = () => {
    this.setState({ applyModalOpen: false, name: '', comment: '' });
  };

  renderJobDetail = () => {
    const { jobs } = this.props;
    const { selectedJob } = this.state;

    return (
      <JobDetail
        onBackButtonClick={this.handleBackButtonClick}
        data={jobs[selectedJob]}
        onApplyClick={this.handleApplyClick}
      />
    );
  };

  handleToolClick = tool => {
    const { fetchJobsAction } = this.props;
    this.setState({ mode: this.modes.list }, () => {
      fetchJobsAction(SETTINGS.JOB_FILTER_KEYS.tool, tool);
    });
  };

  render() {
    const { mode, applyModalOpen, selectedJob, name, comment, permissionDialogOpen } = this.state;
    const { jobs, uploadCvAction, id, uploaded, uploading, fileName, error } = this.props;

    const markers = [];
    Object.keys(jobs).forEach(key => {
      const { geolocation } = jobs[key].company;
      if (geolocation) {
        markers.push({
          id: key,
          lat: Number(jobs[key].company.geolocation.lat),
          lng: Number(jobs[key].company.geolocation.lng),
        });
      }
    });

    return (
      <main>
        <Toolbar sliderDisabled={Boolean(selectedJob)} onToolClick={this.handleToolClick} />
        <div className="screen">
          {applyModalOpen && (
            <Dialog
              fullWidth
              className="applyDialog"
              open={applyModalOpen}
              onClose={this.handleClose}
              aria-labelledby="apply-dialog-title"
              aria-describedby="apply-dialog-description">
              <DialogTitle id="apply-dialog-title"> {jobs[selectedJob].title} </DialogTitle>
              <DialogContent>
                <ApplicationForm
                  error={error}
                  job={id}
                  name={name}
                  uploaded={uploaded}
                  uploading={uploading}
                  comment={comment}
                  onCvUpload={uploadCvAction}
                  onChange={this.handleChange}
                  fileName={fileName}
                />
              </DialogContent>
              <DialogActions>
                <Button onClick={this.handleCancel} color="primary">
                  Cancel
                </Button>
                <Button onClick={this.handleSend} color="primary" autoFocus>
                  Send
                </Button>
              </DialogActions>
            </Dialog>
          )}
          {permissionDialogOpen && (
            <PermissionDialog
              open={permissionDialogOpen}
              onClose={() => this.setState({ permissionDialogOpen: false })}
              text="You already have submitted an application for this job."
            />
          )}
          <div className="list">
            {mode === this.modes.list ? this.renderJobListing() : this.renderJobDetail()}
          </div>
          <div className="mapContainer">
            <Map
              onMarkerClick={idx => this.handleJobClick(idx)}
              width={980}
              height={780}
              markers={markers}
              selected={
                selectedJob
                  ? [
                      Number(jobs[selectedJob].company.geolocation.lat),
                      Number(jobs[selectedJob].company.geolocation.lng),
                    ]
                  : null
              }
            />
          </div>
        </div>
      </main>
    );
  }
}

const mapStateToProps = state => ({
  jobs: state.jobs.data,
  id: state.applications.currentApplication.id || null,
  uploaded: state.applications.uploaded,
  uploading: state.applications.uploading,
  error: state.applications.error,
  fileName: state.applications.file,
});

JobListingScreen.propTypes = {
  id: PropTypes.number,
  jobs: PropTypes.object,
  fetchJobsAction: PropTypes.func,
  match: PropTypes.object,
  deleteApplicationAction: PropTypes.func.isRequired,
  updateApplicationAction: PropTypes.func.isRequired,
  createApplicationAction: PropTypes.func.isRequired,
  uploadCvAction: PropTypes.func.isRequired,
  uploaded: PropTypes.bool.isRequired,
  uploading: PropTypes.bool.isRequired,
  fileName: PropTypes.string.isRequired,
  error: PropTypes.string.isRequired,
};

export default withRouter(
  connect(
    mapStateToProps,
    {
      fetchJobsAction: fetchJobs,
      createApplicationAction: createApplication,
      updateApplicationAction: updateApplication,
      deleteApplicationAction: deleteApplication,
      uploadCvAction: uploadCv,
    },
  )(JobListingScreen),
);
