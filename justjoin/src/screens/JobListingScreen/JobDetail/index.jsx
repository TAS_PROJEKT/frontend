import './style.sass';

import { Button, Icon, IconButton, Paper } from '@material-ui/core';

import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import React from 'react';
import SETTINGS from '../../../config';
import Skill from './Skill/Skill';

const createMarkup = data => ({ __html: data });

const JobDetail = ({ data, onBackButtonClick, onApplyClick, preview }) => (
  <Paper className="detailContainer">
    <div className="row">
      <span className="basicInfoRow">
        <span className="first">
          <div>
            <IconButton variant="contained" color="primary" onClick={onBackButtonClick}>
              <Icon> keyboard_backspace </Icon>
            </IconButton>
          </div>
          <div className="info">
            <h2> General Information</h2>
            <div className="ginfo">
              <div className="salary">
                {data.salaryLow} - {data.salaryHigh} {data.salaryLowCurrency}
              </div>
              <div className="title">{data.title}</div>
              <span className="address">
                <Link to={`${SETTINGS.URLS.COMPANY}/${data.company.id}`}>
                  {data.company.company_name}
                </Link>
                {', '}
                {data.company.location}
              </span>
            </div>
          </div>
        </span>
        {!preview && (
          <Button onClick={onApplyClick} variant="extendedFab" size="small">
            <Icon>mail</Icon>
            Apply
          </Button>
        )}
      </span>
    </div>
    <div className="row">
      <div className="line" />
      <h2>Required skills</h2>
      <span className="skillsRow">
        {Object.keys(data.tools).map(key => (
          <Skill
            id={data.tools[key].id}
            key={data.tools[key].id}
            tool={data.tools[key].tool}
            experience={data.tools[key].experience}
          />
        ))}
      </span>
    </div>
    <div className="descriptionRow row">
      <div className="line" />
      <h2>Description</h2>
      {/* eslint-disable-next-line react/no-danger */}
      <div dangerouslySetInnerHTML={createMarkup(data.description)} />
    </div>
  </Paper>
);

JobDetail.propTypes = {
  data: PropTypes.shape({}),
  onApplyClick: PropTypes.func,
  onBackButtonClick: PropTypes.func.isRequired,
  preview: PropTypes.bool,
};

JobDetail.defaultProps = {
  preview: false,
  onApplyClick: () => {},
};

export default JobDetail;
