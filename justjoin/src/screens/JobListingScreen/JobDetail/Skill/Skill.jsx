import './style.sass';

import PropTypes from 'prop-types';
import React from 'react';
import SETTINGS from '../../../../config';
import { experienceToLevelLabel } from '../../../../helpers';
import uuid4 from 'uuid/v4';

const Skill = ({ tool, experience }) => {
  const dots = [];
  Array(SETTINGS.EXPERIENCE_LIMIT)
    .fill()
    .forEach((_, i) => {
      if (i < experience) {
        dots.push(<span key={uuid4()} className="dot active" />);
      } else {
        dots.push(<span key={uuid4()} className="dot inactive" />);
      }
    });

  const level = experienceToLevelLabel(experience);

  return (
    <div className="skillContainer">
      <div className="skill">
        <div>{tool}</div>
        <div className="dots">{dots}</div>
        <div>{level}</div>
      </div>
    </div>
  );
};

Skill.propTypes = {
  tool: PropTypes.string.isRequired,
  experience: PropTypes.number.isRequired,
};

export default Skill;
