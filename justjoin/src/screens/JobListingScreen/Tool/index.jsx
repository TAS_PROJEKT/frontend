import './style.sass';

import PropTypes from 'prop-types';
import React from 'react';

const Tool = ({ tool }) => (
  <span className="tool">
    <label className="text">{tool}</label>
  </span>
);

Tool.propTypes = {
  tool: PropTypes.string.isRequired,
};

export default Tool;
