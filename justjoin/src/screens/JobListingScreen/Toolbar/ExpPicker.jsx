import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import PropTypes from 'prop-types';
import React from 'react';
import SETTINGS from '../../../config';
import Select from '@material-ui/core/Select';
import { connect } from 'react-redux';
import { fetchJobs } from '../../../store/jobs/actions';

class ExpPicker extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      value: '',
    };
  }

  handleChange = evt => {
    const { value } = evt.target;
    const { fetchJobsAction } = this.props;

    this.setState({ value }, () => {
      fetchJobsAction(SETTINGS.JOB_FILTER_KEYS.exp, value);
    });
  };

  render() {
    const { value } = this.state;

    return (
      <FormControl>
        <InputLabel htmlFor="exp-select">
          <span>Exp. level</span>
        </InputLabel>
        <Select
          className="expSelect"
          onChange={this.handleChange}
          value={value}
          inputProps={{ id: 'exp-select' }}>
          <MenuItem className="menuItem" value="">
            Any
          </MenuItem>
          <MenuItem className="menuItem" value="jr">
            Junior
          </MenuItem>
          <MenuItem className="menuItem" value="md">
            Medium
          </MenuItem>
          <MenuItem className="menuItem" value="sr">
            Senior
          </MenuItem>
        </Select>
      </FormControl>
    );
  }
}

ExpPicker.propTypes = {
  fetchJobsAction: PropTypes.func.isRequired,
};

export default connect(
  null,
  { fetchJobsAction: fetchJobs },
)(ExpPicker);
