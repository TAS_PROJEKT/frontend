import './style.sass';

import ExpPicker from './ExpPicker';
import PropTypes from 'prop-types';
import React from 'react';
import SETTINGS from '../../../config';
import Slider from '@material-ui/lab/Slider';
import ToolWidget from './ToolWidget';
import { connect } from 'react-redux';
import { fetchJobs } from '../../../store/jobs/actions';
import fetchTools from '../../../store/tools/actions';

class Toolbar extends React.PureComponent {
  state = {
    value: 0,
  };

  componentDidMount() {
    const { fetchToolsAction } = this.props;
    fetchToolsAction();
  }

  handleChange = (evt, value) => {
    this.setState({ value }, () => {});
  };

  handleDragEnd = () => {
    const { fetchJobsAction } = this.props;
    const { value } = this.state;
    fetchJobsAction(SETTINGS.JOB_FILTER_KEYS.salary, value);
  };

  render() {
    const { tools, onToolClick, sliderDisabled } = this.props;
    const { value } = this.state;

    return (
      <React.Fragment>
        <span className="toolbar">
          {tools.data &&
            Object.keys(tools.data).map(key => (
              <ToolWidget
                icon={tools.data[key].icon}
                onToolClick={onToolClick}
                key={tools.data[key].id}
                id={tools.data[key].id}
                name={tools.data[key].tool}
              />
            ))}
          <span className="slider">
            Salary {<span>{parseInt(value, 10)}+</span>}
            <div className="slider">
              <Slider
                disabled={sliderDisabled}
                onChange={this.handleChange}
                value={value}
                onDragEnd={this.handleDragEnd}
                min={0}
                max={30000}
              />
            </div>
          </span>
          <div className="rightSide">
            <ExpPicker />
          </div>
        </span>
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => ({
  tools: state.tools,
});

Toolbar.propTypes = {
  fetchToolsAction: PropTypes.func.isRequired,
  tools: PropTypes.shape({}),
  onToolClick: PropTypes.func.isRequired,
  fetchJobsAction: PropTypes.func.isRequired,
  sliderDisabled: PropTypes.bool.isRequired,
};

export default connect(
  mapStateToProps,
  { fetchToolsAction: fetchTools, fetchJobsAction: fetchJobs },
)(Toolbar);
