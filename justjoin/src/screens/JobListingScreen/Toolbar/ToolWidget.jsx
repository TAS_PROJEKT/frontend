import './style.sass';

import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import React from 'react';
import { formatToUrl } from '../../../helpers';

const ToolWidget = ({ name, id, icon, onToolClick }) => (
  /* eslint-disable-next-line */
  <div onClick={() => onToolClick(id)} className="widgetContainer">
    {' '}
    <Link to={`/${formatToUrl(name)}`}>
      <img className="icon" src={icon} alt="" />
    </Link>
    <div>{name}</div>
  </div>
);

ToolWidget.propTypes = {
  name: PropTypes.string.isRequired,
  id: PropTypes.number,
  onToolClick: PropTypes.func.isRequired,
  icon: PropTypes.string.isRequired,
};

export default ToolWidget;
