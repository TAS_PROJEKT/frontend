import './style.sass';

import { Icon } from '@material-ui/core';
import PropTypes from 'prop-types';
import React from 'react';
import SETTINGS from '../../../../../config';
import uuid4 from 'uuid/v4';

class RatingPicker extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      level: 0,
    };
  }

  render() {
    const { onRatingChange } = this.props;
    const dynamicStars = [];
    const { level } = this.state;

    Array(SETTINGS.RATING_LIMIT)
      .fill()
      .forEach((_, i) => {
        dynamicStars.push(
          <Icon
            onMouseOver={() => {
              this.setState({ level: i + 1 }, () => {
                onRatingChange(i + 1);
              });
            }}
            key={uuid4()}
            className={level > i ? 'filledStar' : 'star'}>
            star_rate
          </Icon>,
        );
      });

    return <div className="ratingPicker">{dynamicStars}</div>;
  }
}

RatingPicker.propTypes = {
  onRatingChange: PropTypes.func.isRequired,
};

export default RatingPicker;
