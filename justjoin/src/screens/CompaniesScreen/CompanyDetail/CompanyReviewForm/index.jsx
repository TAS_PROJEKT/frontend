import './style.sass';

import PropTypes from 'prop-types';
import RatingPicker from './RatingPicker';
import React from 'react';
import { TextField } from '@material-ui/core';

const CompanyReviewForm = ({ text, onChangeText, onRatingChange }) => (
  <div className="reviewForm">
    <TextField
      required
      multiline
      fullWidth
      placeholder="Review text"
      onChange={evt => onChangeText(evt.target.value)}
      value={text}
    />
    <RatingPicker onRatingChange={onRatingChange} />
  </div>
);

CompanyReviewForm.propTypes = {
  onChangeText: PropTypes.func.isRequired,
  text: PropTypes.string.isRequired,
  onRatingChange: PropTypes.func.isRequired,
};

export default CompanyReviewForm;
