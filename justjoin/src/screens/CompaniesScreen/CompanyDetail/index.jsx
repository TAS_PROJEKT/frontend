import './style.sass';

import * as actions from '../../../store/companies/actions';

import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  Icon,
  IconButton,
  Paper,
} from '@material-ui/core';
import React, { PureComponent } from 'react';

import CompanyReview from './CompanyReview';
import CompanyReviewForm from './CompanyReviewForm';
import Map from '../../../common/Map';
import PermissionDialog from '../../../common/PermissionDialog';
import PropTypes from 'prop-types';
import ReviewRating from './CompanyReview/ReviewRating';
import SETTINGS from '../../../config';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';

class CompanyDetail extends PureComponent {
  state = {
    reviewRating: 3,
    text: '',
    reviewDialogOpen: false,
    permissionDialogOpen: false,
  };

  componentDidMount() {
    const { match, fetchCompanyDetail } = this.props;
    const { id } = match.params;
    fetchCompanyDetail(id);
  }

  handleDialogOpen = () => {
    const { createReview, company } = this.props;

    createReview(company.id).then(status => {
      if (status === 403) {
        this.setState({ permissionDialogOpen: true });
      } else {
        this.setState({ reviewDialogOpen: true });
      }
    });
  };

  handleTextChange = text => {
    this.setState({ text });
  };

  handleCancel = () => {
    const { deleteReview } = this.props;
    deleteReview();
    this.setState({ reviewDialogOpen: false });
  };

  handleRatingChange = reviewRating => {
    this.setState({ reviewRating });
  };

  handleSend = () => {
    const { reviewRating, text } = this.state;
    const { updateReview } = this.props;

    updateReview({ rating: reviewRating, review: text });
    this.setState({ reviewDialogOpen: false });
  };

  render() {
    const { company, history } = this.props;
    const { reviewDialogOpen, permissionDialogOpen, text } = this.state;

    const {
      companyReview,
      geolocation,
      description,
      companyName,
      companySize,
      logo,
      siteUrl,
      location,
      rating,
    } = company;

    let marker = [];
    if (geolocation && 'lat' in geolocation && 'lng' in geolocation) {
      const { lat, lng } = geolocation;
      marker = [{ lat: Number(lat), lng: Number(lng) }];
    }

    const reviewList = [];

    companyReview.forEach(review => {
      reviewList.push(
        <CompanyReview
          key={review.id}
          author={review.owner}
          text={review.review}
          rating={review.rating}
          date={review.date}
        />,
      );
    });

    return (
      <div className="companyDetailContainer">
        <Paper className="mainPaper">
          <span className="topIcons">
            <IconButton
              variant="contained"
              color="primary"
              onClick={() => history.push(SETTINGS.URLS.COMPANIES)}>
              <Icon> keyboard_backspace </Icon>
            </IconButton>
            All companies
            <Button
              onClick={this.handleDialogOpen}
              className="reviewIcon"
              variant="extendedFab"
              size="small">
              <Icon>message</Icon>
              Write a review
            </Button>
          </span>
          <h1>{companyName}</h1>
          <div className="row description">
            <span>
              <img className="logo" src={logo} alt="" />
              <h3>About: </h3>
              <p>{description}</p>
            </span>
          </div>
          <div className="row companyUrl">
            <h3>Company website: </h3>
            <p>
              <a href={siteUrl}>{siteUrl}</a>
            </p>
          </div>
          <div className="row companySize">
            <h3>Company size: </h3>
            <p>
              {companySize}
              <span className="companySizeIcon">
                <Icon>group</Icon>
              </span>
            </p>
          </div>
          <div className="row companyRating">
            <h3>Company rating: </h3>
            <p>
              <ReviewRating rating={rating} />{' '}
            </p>
          </div>
          <div className="row companyLocation">
            <h3>Location: </h3>
            <p>{location}</p>
          </div>
          {geolocation && <Map width={700} height={400} markers={marker} />}
          <h1> Reviews </h1>
          {reviewList.length > 0
            ? reviewList
            : 'This company has not received any reviews yet.'}{' '}
        </Paper>
        <Dialog className="reviewDialog" open={reviewDialogOpen} fullWidth>
          <DialogTitle>Write a review for {companyName}</DialogTitle>
          <DialogContent>
            <CompanyReviewForm
              onChangeText={this.handleTextChange}
              text={text}
              onRatingChange={this.handleRatingChange}
            />
          </DialogContent>
          <DialogActions>
            <Button onClick={this.handleCancel}>Cancel</Button>
            <Button onClick={this.handleSend}>Send</Button>
          </DialogActions>
        </Dialog>
        <PermissionDialog
          onClose={() => this.setState({ permissionDialogOpen: false })}
          open={permissionDialogOpen}
          text="You already have submitted a review for this company."
        />
      </div>
    );
  }
}

CompanyDetail.propTypes = {
  match: PropTypes.shape({}),
  fetchCompanyDetail: PropTypes.func.isRequired,
  company: PropTypes.shape({ companyName: PropTypes.string, companyReview: PropTypes.array }),
  history: PropTypes.shape({}),
  createReview: PropTypes.func.isRequired,
  deleteReview: PropTypes.func.isRequired,
  updateReview: PropTypes.func.isRequired,
};

CompanyDetail.defaultProps = {
  company: { companyName: '', companyReview: [] },
};

const mapStateToProps = (state, ownProps) => {
  const { id } = ownProps.match.params;

  return {
    company: state.companies.data[id],
    pending: state.companies.pending,
    error: state.companies.error,
  };
};

export default withRouter(
  connect(
    mapStateToProps,
    {
      fetchCompanyDetail: actions.fetchCompanyDetail,
      createReview: actions.createReview,
      updateReview: actions.updateReview,
      deleteReview: actions.deleteReview,
    },
  )(CompanyDetail),
);
