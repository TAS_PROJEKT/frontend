import './style.sass';

import { Button, Paper } from '@material-ui/core';
import React, { PureComponent } from 'react';

import PropTypes from 'prop-types';
import ReviewRating from './ReviewRating';
import { jobDateToLabel } from '../../../../helpers';

class CompanyReview extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      expanded: false,
    };
  }

  render() {
    const { author, rating, text, date } = this.props;
    const { expanded } = this.state;

    return (
      <Paper className="reviewContainer">
        <div className="row reviewAuthor"> {author} </div>
        <div className="row reviewRating">
          {' '}
          <ReviewRating rating={rating} />
          <label> {jobDateToLabel(date)}</label>
        </div>
        <div className="row reviewText">
          {' '}
          {expanded ? <p> {text} </p> : <p> {text.substr(0, 256)} </p>}
          {text.length > 256 && (
            <Button
              key="expanderBtn"
              onClick={() => this.setState(prevState => ({ expanded: !prevState.expanded }))}>
              {' '}
              {expanded ? 'Read less...' : 'Read more...'}
            </Button>
          )}
        </div>
      </Paper>
    );
  }
}

CompanyReview.propTypes = {
  author: PropTypes.string,
  rating: PropTypes.number,
  text: PropTypes.string,
  date: PropTypes.string,
};

CompanyReview.defaultProps = {
  author: 'Anoynmous',
  rating: 3,
  text: 'No review text available.',
  date: '',
};

export default CompanyReview;
