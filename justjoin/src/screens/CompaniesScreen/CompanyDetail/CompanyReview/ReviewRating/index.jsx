import './style.sass';

import Icon from '@material-ui/core/Icon';
import PropTypes from 'prop-types';
import React from 'react';
import SETTINGS from '../../../../../config';
import uuid4 from 'uuid/v4';

const ReviewRating = ({ rating }) => {
  const stars = [];
  Array(SETTINGS.RATING_LIMIT)
    .fill()
    .forEach((_, i) => {
      if (i < rating) {
        stars.push(
          <Icon key={uuid4()} className="filledStar">
            {' '}
            star_rate{' '}
          </Icon>,
        );
      }
    });
  return <span className="stars">{stars}</span>;
};

ReviewRating.propTypes = {
  rating: PropTypes.number,
};

ReviewRating.defaultProps = {
  rating: 3,
};

export default ReviewRating;
