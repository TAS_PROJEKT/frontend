import './style.sass';

import { Card, CardActionArea, CardContent } from '@material-ui/core';
import React, { PureComponent } from 'react';

import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { fetchCompanies } from '../../store/companies/actions';
import { withRouter } from 'react-router-dom';

export class CompaniesScreen extends PureComponent {
  componentDidMount() {
    const { fetchCompaniesAction } = this.props;
    fetchCompaniesAction();
  }

  handleCompanyClick = company => {
    const { history } = this.props;
    const { id } = company;

    history.push(`/company/${id}`);
  };

  render() {
    const { companies } = this.props;

    const companyList = [];
    Object.keys(companies).forEach(key =>
      companyList.push(
        <Card className="card" key={key}>
          <CardActionArea onClick={() => this.handleCompanyClick(companies[key])}>
            <CardContent>
              <img className="cardLogo" src={companies[key].logo} alt="" />
              <h2>{companies[key].companyName}</h2>
              <p>{companies[key].description}</p>
            </CardContent>
          </CardActionArea>
          ,
        </Card>,
      ),
    );

    return <div className="cardScreen">{companyList}</div>;
  }
}

const mapStateToProps = state => ({ companies: state.companies.data });

CompaniesScreen.propTypes = {
  companies: PropTypes.shape({}).isRequired,
  fetchCompaniesAction: PropTypes.func.isRequired,
  history: PropTypes.shape({}),
};

export default withRouter(
  connect(
    mapStateToProps,
    { fetchCompaniesAction: fetchCompanies },
  )(CompaniesScreen),
);
