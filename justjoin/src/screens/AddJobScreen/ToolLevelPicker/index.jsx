import './style.sass';

import { Icon, IconButton } from '@material-ui/core';

import PropTypes from 'prop-types';
import React from 'react';
import SETTINGS from '../../../config';
import uuid4 from 'uuid/v4';

class ToolLevelPicker extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      level: 0,
      confirmed: false,
    };
  }

  render() {
    const { level, confirmed } = this.state;
    const { label, onToolDelete, onToolConfirm, id } = this.props;
    const dynamicDots = [];

    Array(SETTINGS.EXPERIENCE_LIMIT)
      .fill()
      .forEach((_, i) => {
        dynamicDots.push(
          <span
            onMouseOver={() => this.setState({ level: i + 1 })}
            onClick={() => {
              this.setState({ level: i + 1, confirmed: true }, () => {
                onToolConfirm(id, i + 1);
              });
            }}
            key={uuid4()}
            className={level > i ? 'dynamicDot active' : 'dynamicDot'}
          />,
        );
      });

    return (
      <div className="skill">
        {label}
        {confirmed && (
          <IconButton onClick={() => onToolDelete(id)}>
            <Icon style={{ fontSize: '1rem', color: '#cc0000' }}>remove_circle</Icon>
          </IconButton>
        )}
        <div>{dynamicDots}</div>
      </div>
    );
  }
}

ToolLevelPicker.propTypes = {
  id: PropTypes.number.isRequired,
  label: PropTypes.string.isRequired,
  onToolDelete: PropTypes.func.isRequired,
  onToolConfirm: PropTypes.func.isRequired,
};

export default ToolLevelPicker;
