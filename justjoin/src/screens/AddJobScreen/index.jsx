import './style.sass';
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';

import * as jobActions from '../../store/jobs/actions';
import * as userActions from '../../store/auth/actions';

import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  FormControl,
  Icon,
  IconButton,
  InputLabel,
  MenuItem,
  Paper,
  Select,
  TextField,
} from '@material-ui/core';
import { EditorState, convertToRaw } from 'draft-js';

import { Editor } from 'react-draft-wysiwyg';
import Map from '../../common/Map';
import PropTypes from 'prop-types';
import React from 'react';
import SETTINGS from '../../config';
import ToolLevelPicker from './ToolLevelPicker';
import { connect } from 'react-redux';
import draftToHtml from 'draftjs-to-html';
import fetchTools from '../../store/tools/actions';
import { omit } from 'lodash';
import { withRouter } from 'react-router-dom';

class AddJobScreen extends React.PureComponent {
  companyFields = ['siteUrl', 'companySize', 'companyName'];

  initialState = {
    title: '',
    employmentType: '',
    salaryLow: '',
    salaryHigh: '',
    contactEmail: '',
    salaryHighCurrency: '',
    description: EditorState.createEmpty(),
    overallExperience: '',
    tools: [],
    toolExperiences: {},
    dialogOpen: false,
  };

  constructor(props) {
    super(props);
    this.state = this.initialState;
  }

  componentDidMount() {
    const { tools, fetchToolsAction, fetchUserData } = this.props;

    if (!Object.keys(tools.data).length) {
      fetchToolsAction();
    }

    fetchUserData();
  }

  handleChange = (name, val) => {
    if (this.companyFields.includes(name)) {
      this.setState(prevState => ({
        ...prevState,
        company: { ...prevState.company, [name]: val },
      }));
    } else {
      this.setState({ [name]: val });
    }
  };

  onEditorStateChange = editorState => {
    this.setState({ description: editorState });
  };

  renderToolMenu = () => {
    const { tools } = this.props;
    const toolMenu = [];
    Object.keys(tools.data).forEach(key =>
      toolMenu.push(
        <MenuItem className="menuItem" key={key} value={tools.data[key].id}>
          {tools.data[key].tool}
        </MenuItem>,
      ),
    );
    return toolMenu;
  };

  renderToolWidgets = () => {
    const { tools } = this.state;
    const { props } = this;
    const toolWidgets = [];
    tools.forEach(id => {
      toolWidgets.push(
        <ToolLevelPicker
          onToolConfirm={(toolId, exp) =>
            this.setState(prevState => ({
              toolExperiences: { ...prevState.toolExperiences, [toolId]: exp },
            }))
          }
          onToolDelete={idx => {
            const newTools = [];
            tools.forEach(tool => {
              if (tool !== idx) {
                newTools.push(tool);
              }
            });
            this.setState({ tools: newTools });
          }}
          id={id}
          key={id}
          label={props.tools.data[id].tool}
        />,
      );
    });
    return toolWidgets;
  };

  handleSubmit = evt => {
    evt.preventDefault();
    const { description, tools, toolExperiences } = this.state;
    const { props } = this;

    const preparedTools = [];
    tools.forEach(id => {
      const experience = toolExperiences[id] || SETTINGS.DEFAULT_EXPERIENCE;
      preparedTools.push({ tool: props.tools.data[id].tool, experience });
    });

    const editorStateHtml = draftToHtml(convertToRaw(description.getCurrentContent()));
    const { postJob } = this.props;
    postJob({
      ...omit(this.state, ['toolExperiences', 'dialogOpen']),
      description: editorStateHtml,
      tools: preparedTools,
    }).then(() => {
      this.setState({ dialogOpen: true });
    });
  };

  handleContinue = () => {
    this.setState(this.initialState);
  };

  handleRedirect = () => {
    const { history } = this.props;
    history.push(SETTINGS.URLS.DASHBOARD);
  };

  render() {
    const {
      title,
      employmentType,
      salaryLow,
      salaryHigh,
      salaryHighCurrency,
      description,
      overallExperience,
      tools,
      contactEmail,
      dialogOpen,
    } = this.state;

    const { history, companyName, companySize, logo, siteUrl, geolocation } = this.props;

    if (!companyName) {
      return <div className="noCompany"> Please add a company first! Go to the Dashboard.</div>;
    }
    let marker = [];
    if (geolocation && 'lat' in geolocation && 'lng' in geolocation) {
      const { lat, lng } = geolocation;
      marker = [{ lat: Number(lat), lng: Number(lng) }];
    }
    return (
      <div className="addJobContainer">
        <Paper className="paper">
          <IconButton
            className="btn backBtn"
            variant="contained"
            color="primary"
            onClick={() => history.push('/')}>
            <Icon fontSize="large"> keyboard_backspace </Icon>
          </IconButton>
          <form onSubmit={this.handleSubmit}>
            <div className="row">
              <div className="logoContainer">
                <img src={`${SETTINGS.API_URLS.BACKEND}${logo}`} alt="No company logo" />
              </div>
            </div>
            <div className="row">
              <TextField
                disabled
                onChange={evt => this.handleChange('companyName', evt.target.value)}
                value={companyName}
                placeholder="Company name"
                className="input"
              />
              <TextField
                disabled
                onChange={evt => this.handleChange('siteUrl', evt.target.value)}
                value={siteUrl}
                placeholder="Company website"
                className="input"
              />
              <TextField
                disabled
                onChange={evt => this.handleChange('companySize', evt.target.value)}
                value={companySize}
                placeholder="Company size"
                className="input"
              />
            </div>
            <div className="row">
              <TextField
                required
                label={<span>Title</span>}
                onChange={evt => this.handleChange('title', evt.target.value)}
                value={title}
                placeholder="Title"
                className="input"
              />
              <FormControl className="input">
                <InputLabel htmlFor="ov-exp-select">
                  <span>Overall experience</span>
                </InputLabel>
                <Select
                  type="select"
                  inputProps={{ id: 'ov-exp-select' }}
                  value={overallExperience}
                  onChange={evt => this.handleChange('overallExperience', evt.target.value)}>
                  <MenuItem className="menuItem" value="jr">
                    Junior
                  </MenuItem>
                  <MenuItem className="menuItem" value="md">
                    Medium
                  </MenuItem>
                  <MenuItem className="menuItem" value="sr">
                    Senior
                  </MenuItem>
                </Select>
              </FormControl>
              <FormControl className="input">
                <InputLabel htmlFor="emp-type-select">
                  <span>Employment Type </span>
                </InputLabel>
                <Select
                  onChange={evt => this.handleChange('employmentType', evt.target.value)}
                  value={employmentType}>
                  <MenuItem className="menuItem" value="b2b">
                    B2B
                  </MenuItem>
                  <MenuItem className="menuItem" value="perm">
                    Permanent
                  </MenuItem>
                </Select>
              </FormControl>
            </div>
            <div className="row">
              <TextField
                required
                label={<span>Salary from (net)</span>}
                onChange={evt => this.handleChange('salaryLow', evt.target.value)}
                value={salaryLow}
                placeholder="Salary from (net)"
                className="input"
              />
              <TextField
                required
                onChange={evt => this.handleChange('salaryHigh', evt.target.value)}
                value={salaryHigh}
                label={<span>Salary to (net)</span>}
                placeholder="Salary to (net)"
                className="input"
              />
              <FormControl className="input">
                <InputLabel htmlFor="currency-select">
                  <span>Currency</span>
                </InputLabel>
                <Select
                  onChange={evt => this.handleChange('salaryHighCurrency', evt.target.value)}
                  value={salaryHighCurrency}>
                  <MenuItem className="menuItem" value="EUR">
                    EUR
                  </MenuItem>
                  <MenuItem className="menuItem" value="PLN">
                    PLN
                  </MenuItem>
                </Select>
              </FormControl>
            </div>
            <div className="row">
              <TextField
                required
                label={<span>Contact email</span>}
                onChange={evt => this.handleChange('contactEmail', evt.target.value)}
                value={contactEmail}
                placeholder="Contact email"
                className="input alignStart"
              />
            </div>
            <p className="rowHeader"> Required skills </p>
            <div className="row">
              <FormControl className="skillSelect">
                <InputLabel htmlFor="skill-select">
                  <span>Skills</span>
                </InputLabel>
                <Select
                  multiple
                  onChange={evt => this.handleChange('tools', evt.target.value)}
                  value={tools}>
                  {this.renderToolMenu()}
                </Select>
              </FormControl>
            </div>
            {tools.length > 0 && (
              <div className="row">
                <span className="skillWidgets">{this.renderToolWidgets()}</span>
              </div>
            )}
            <p className="rowHeader"> Job Description</p>
            <div className="row">
              <Editor
                editorClassName="editor"
                editorState={description}
                onEditorStateChange={this.onEditorStateChange}
              />
            </div>

            <p className="rowHeader"> Location</p>
            <div className="row">
              <Map width={800} height={200} markers={marker} />
            </div>

            <div className="row">
              <Button className="btn" size="large" fullWidth type="submit">
                Submit
              </Button>
            </div>
          </form>
        </Paper>

        {dialogOpen && (
          <Dialog
            open={dialogOpen}
            onClose={this.handleClose}
            aria-labelledby="submit-dialog-title"
            aria-describedby="submit-dialog-description">
            <DialogTitle id="submit-dialog-title">Job offer added</DialogTitle>
            <DialogContent>
              <DialogContentText id="alert-dialog-description">
                Thank you for posting your job offer. You can add another one or view your offers on
                dashboard.
              </DialogContentText>
            </DialogContent>
            <DialogActions>
              <Button onClick={this.handleRedirect} color="primary">
                Go to dashboard
              </Button>
              <Button onClick={this.handleContinue} color="primary" autoFocus>
                Add another job
              </Button>
            </DialogActions>
          </Dialog>
        )}
      </div>
    );
  }
}

AddJobScreen.propTypes = {
  history: PropTypes.shape({}),
  postJob: PropTypes.func.isRequired,
  tools: PropTypes.shape({}),
  fetchToolsAction: PropTypes.func.isRequired,
  fetchUserData: PropTypes.func.isRequired,
  geolocation: PropTypes.shape({ lat: PropTypes.string, lng: PropTypes.string }),
  companyName: PropTypes.string,
  companySize: PropTypes.string,
  siteUrl: PropTypes.string,
  logo: PropTypes.string,
};

const mapStateToProps = state => ({
  tools: state.tools,
  companyName: state.auth.data.company.companyName || '',
  siteUrl: state.auth.data.company.siteUrl || '',
  companySize: state.auth.data.company.companySize || '',
  logo: state.auth.data.company.logo || '',
  geolocation: state.auth.data.company.geolocation || null,
});

export default withRouter(
  connect(
    mapStateToProps,
    {
      postJob: jobActions.postJob,
      fetchToolsAction: fetchTools,
      fetchUserData: userActions.fetchUserData,
    },
  )(AddJobScreen),
);
