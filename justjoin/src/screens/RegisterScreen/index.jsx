import './style.sass';

import * as actions from '../../store/auth/actions';

import { Button, Dialog, DialogActions, DialogContent, DialogTitle } from '@material-ui/core';
import React, { PureComponent } from 'react';

import PropTypes from 'prop-types';
import RegisterForm from './RegisterForm';
import { connect } from 'react-redux';

export class RegisterScreen extends PureComponent {
  state = {
    email: '',
    password: '',
    name: '',
    openDialog: false,
  };

  handleChange = (name, val) => {
    this.setState({ [name]: val });
  };

  handleSubmit = () => {
    const { register } = this.props;
    const { openDialog, ...rest } = this.state;
    register(rest).then(status => {
      if (status && status === 201) {
        this.setState({ openDialog: true });
      }
    });
  };

  handleDialogClose = () => {
    this.setState({ openDialog: false });
  };

  render() {
    const { email, password, name, openDialog } = this.state;
    return (
      <div className="formContainer">
        <RegisterForm
          onChange={this.handleChange}
          email={email}
          name={name}
          password={password}
          onSubmit={this.handleSubmit}
        />
        <Dialog open={openDialog}>
          <DialogTitle>
            <span>Registration complete</span>
          </DialogTitle>
          <DialogContent>We have sent you an email with confirmation link.</DialogContent>
          <DialogActions>
            <Button onClick={this.handleDialogClose}>Ok</Button>
          </DialogActions>
        </Dialog>
      </div>
    );
  }
}
const mapStateToProps = state => ({ token: state.token });

RegisterScreen.propTypes = {
  register: PropTypes.func.isRequired,
};

export default connect(
  mapStateToProps,
  {
    register: actions.register,
  },
)(RegisterScreen);
