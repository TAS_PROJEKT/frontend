import Button from '@material-ui/core/Button';
import FormControl from '@material-ui/core/FormControl';
import PropTypes from 'prop-types';
import React from 'react';
import TextField from '@material-ui/core/TextField';

const RegisterForm = ({ onChange, onSubmit, email, password, name }) => (
  <FormControl>
    <TextField
      required
      type="email"
      placeholder="E-mail"
      value={email}
      onChange={evt => onChange('email', evt.target.value)}
    />
    <TextField
      required
      placeholder="Name"
      value={name}
      onChange={evt => onChange('name', evt.target.value)}
    />
    <TextField
      required
      placeholder="Password"
      type="password"
      value={password}
      onChange={evt => onChange('password', evt.target.value)}
    />
    <Button onClick={onSubmit} type="submit">
      Register
    </Button>
  </FormControl>
);

RegisterForm.propTypes = {
  onChange: PropTypes.func.isRequired,
  onSubmit: PropTypes.func.isRequired,
  email: PropTypes.string.isRequired,
  password: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
};

export default RegisterForm;
