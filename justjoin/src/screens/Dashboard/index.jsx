import './style.sass';

import React, { PureComponent } from 'react';

import ApplicationListing from './ApplicationListing';
import { Button } from '@material-ui/core';
import JobOfferListing from './JobOfferListing';
import MyCompany from './MyCompany';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { fetchApplications } from '../../store/applications/actions';
import { fetchCompanyJobs } from '../../store/jobs/actions';

class Dashboard extends PureComponent {
  modes = {
    jobOfferListing: 'JobOffersListing',
    applicationListing: 'ApplicationListing',
    myCompany: 'MyCompany',
  };

  constructor(props) {
    super(props);
    this.state = { mode: this.modes.jobOfferListing };
  }

  componentDidMount() {
    const { fetchApplicationsAction, fetchJobsAction } = this.props;
    fetchApplicationsAction();
    fetchJobsAction();
  }

  render() {
    const { mode } = this.state;
    const { applications, jobs } = this.props;
    let activeScreen;

    if (mode === this.modes.jobOfferListing) {
      activeScreen = <JobOfferListing jobs={jobs} />;
    } else if (mode === this.modes.applicationListing) {
      activeScreen = <ApplicationListing apps={applications} jobs={jobs} />;
    } else if (mode === this.modes.myCompany) {
      activeScreen = <MyCompany />;
    } else {
      activeScreen = null;
    }

    return (
      <div className="dashboard">
        <div className="btnContainer">
          <span>
            <Button
              className={mode === this.modes.jobOfferListing ? 'btn selected' : 'btn'}
              variant="contained"
              color="primary"
              onClick={() => {
                this.setState({ mode: this.modes.jobOfferListing });
              }}>
              My job offers
            </Button>
            <Button
              className={mode === this.modes.applicationListing ? 'btn selected' : 'btn'}
              variant="contained"
              color="primary"
              onClick={() => {
                this.setState({ mode: this.modes.applicationListing });
              }}>
              Applications received
            </Button>
            <Button
              className={mode === this.modes.myCompany ? 'btn selected' : 'btn'}
              variant="contained"
              color="primary"
              onClick={() => {
                this.setState({ mode: this.modes.myCompany });
              }}>
              My company
            </Button>
          </span>
        </div>
        <div>{activeScreen}</div>
      </div>
    );
  }
}

Dashboard.propTypes = {
  fetchApplicationsAction: PropTypes.func.isRequired,
  fetchJobsAction: PropTypes.func.isRequired,
  applications: PropTypes.shape({}),
  jobs: PropTypes.shape({}),
};

const mapStateToProps = state => ({
  applications: state.applications.data,
  id: state.auth.data.company.id,
  jobs: state.jobs.data,
});

export default connect(
  mapStateToProps,
  { fetchApplicationsAction: fetchApplications, fetchJobsAction: fetchCompanyJobs },
)(Dashboard);
