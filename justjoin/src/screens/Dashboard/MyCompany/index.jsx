import './style.sass';

import * as actions from '../../../store/companies/actions';

import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  Paper,
  TextField,
} from '@material-ui/core';
import React, { PureComponent } from 'react';

import FileLoader from '../../../common/FileLoader';
import Map from '../../../common/Map';
import PropTypes from 'prop-types';
import SETTINGS from '../../../config';
import { connect } from 'react-redux';
import { fetchUserData } from '../../../store/auth/actions';

class MyCompany extends PureComponent {
  modes = {
    add: 'add',
    edit: 'edit',
  };

  /* eslint-disable react/no-unused-state */
  constructor(props) {
    super(props);
    this.state = {
      geolocation: null,
      companyName: '',
      companySize: '',
      description: '',
      siteUrl: '',
      logo: '',
      id: null,
      mode: this.modes.add,
      dialogOpen: false,
    };
  }

  componentDidMount() {
    const { fetchUserDataAction } = this.props;
    fetchUserDataAction().then(response => {
      if ('company' in response && response.company && Object.keys(response.company).length > 0) {
        const {
          companyName,
          companySize,
          geolocation,
          siteUrl,
          id,
          logo,
          description,
        } = response.company;
        this.setState({
          companyName,
          companySize,
          description,
          geolocation,
          siteUrl,
          logo,
          id,
          mode: this.modes.edit,
        });
      }
    });
  }

  handleClose = () => {
    this.setState({ dialogOpen: false });
  };

  handleChange = (name, value) => {
    this.setState({ [name]: value });
  };

  handleFileUpload = file => {
    const { id } = this.state;
    const { uploadLogo } = this.props;

    if (id) {
      uploadLogo(file, id).then(res => {
        this.setState({ logo: res.logo });
      });
    }
  };

  handleSubmit = evt => {
    evt.preventDefault();
    const { addCompany, editCompany } = this.props;
    const { mode, logo, dialogOpen, ...rest } = this.state;
    if (mode === this.modes.add) {
      addCompany(rest).then(res => {
        this.setState({ mode: this.modes.edit, id: res.id });
      });
    } else {
      const { id } = this.state;
      if (id) {
        editCompany(rest, id).then(() => {
          this.setState({ dialogOpen: true });
        });
      }
    }
  };

  render() {
    const {
      mode,
      companyName,
      companySize,
      siteUrl,
      geolocation,
      logo,
      dialogOpen,
      description,
    } = this.state;
    const { uploaded, uploading, fileName, error } = this.props;
    return (
      <Paper className="myCompanyContainer">
        <form onSubmit={this.handleSubmit}>
          <div className="image">
            {logo && <img src={`${SETTINGS.API_URLS.BACKEND}${logo}`} alt="" />}
          </div>
          {mode === this.modes.edit && (
            <div className="file">
              <FileLoader
                extensions={SETTINGS.IMAGE_EXTENSIONS}
                onFileUpload={this.handleFileUpload}
                label={`${logo ? 'Edit' : 'Upload'} logo`}
                uploaded={uploaded}
                uploading={uploading}
                fileName={fileName}
                error={error}
              />
            </div>
          )}
          <div className="companyLeftSide">
            <TextField
              required
              onChange={evt => this.handleChange('companyName', evt.target.value)}
              placeholder="Company name"
              value={companyName}
              label={<span>Company name</span>}
            />
            <TextField
              required
              onChange={evt => this.handleChange('description', evt.target.value)}
              placeholder="Description"
              value={description}
              label={<span>Description</span>}
            />
            <TextField
              required
              type="number"
              value={companySize}
              onChange={evt => this.handleChange('companySize', evt.target.value)}
              placeholder="Company size"
              label={<span>Company size</span>}
            />
            <TextField
              required
              value={siteUrl}
              type="url"
              onChange={evt => this.handleChange('siteUrl', evt.target.value)}
              placeholder="Company website"
              label={<span>Company website</span>}
            />
          </div>
          <div className="companyRightSide">
            <Map
              markers={
                geolocation && 'lat' in geolocation && 'lng' in geolocation
                  ? [{ lat: Number(geolocation.lat), lng: Number(geolocation.lng) }]
                  : []
              }
              onMapClick={point => this.setState({ geolocation: point })}
              width={500}
              height={500}
              buttonLabel={mode === this.modes.add ? 'Add a location' : 'Edit location'}
              interactive
            />
          </div>
          <Button className="btn" type="submit">
            {' '}
            {mode === this.modes.add ? 'Add' : 'Save'}{' '}
          </Button>
        </form>
        {dialogOpen && (
          <Dialog
            open={dialogOpen}
            onClose={this.handleClose}
            aria-labelledby="comp-save-dialog-title"
            aria-describedby="comp-save-dialog-description">
            <DialogTitle id="comp-save-dialog-title">Company saved</DialogTitle>
            <DialogContent>
              <DialogContentText id="comp-save-dialog-description">
                Your company data has been saved.
              </DialogContentText>
            </DialogContent>
            <DialogActions>
              <Button onClick={this.handleClose} color="primary" autoFocus>
                OK
              </Button>
            </DialogActions>
          </Dialog>
        )}
      </Paper>
    );
  }
}

MyCompany.propTypes = {
  addCompany: PropTypes.func.isRequired,
  editCompany: PropTypes.func.isRequired,
  uploadLogo: PropTypes.func.isRequired,
  fetchUserDataAction: PropTypes.func.isRequired,
  error: PropTypes.string.isRequired,
  uploading: PropTypes.bool.isRequired,
  uploaded: PropTypes.bool.isRequired,
  fileName: PropTypes.string.isRequired,
};

const mapStateToProps = state => ({
  userData: state.auth.data.companies,
  companyName: state.auth.data.company ? state.auth.data.company.companyName : '',
  description: state.auth.data.company ? state.auth.data.company.description : '',
  error: state.companies.error,
  fileName: state.companies.fileName,
  uploading: state.companies.uploading,
  uploaded: state.companies.uploaded,
});

export default connect(
  mapStateToProps,
  {
    addCompany: actions.addCompany,
    editCompany: actions.editCompany,
    uploadLogo: actions.uploadLogo,
    fetchUserDataAction: fetchUserData,
  },
)(MyCompany);
