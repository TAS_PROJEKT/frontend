import './style.sass';

import React, { PureComponent } from 'react';

import JobDetail from '../../JobListingScreen/JobDetail';
import JobListItem from '../../JobListingScreen/JobListing/JobListItem';
import PropTypes from 'prop-types';

class JobOfferListing extends PureComponent {
  state = {
    selectedJob: null,
  };

  render() {
    const { jobs } = this.props;
    const { selectedJob } = this.state;
    const jobList = [];

    Object.keys(jobs).forEach(key => {
      jobList.push(
        <JobListItem
          key={key}
          salaryLow={jobs[key].salaryLow}
          salaryHigh={jobs[key].salaryHigh}
          contactEmail={jobs[key].salaryHigh}
          tools={jobs[key].tools}
          currency={jobs[key].salaryHighCurrency}
          title={jobs[key].title}
          dateCreated={jobs[key].created}
          companyName={jobs[key].company.company_name}
          companyLogo={jobs[key].company.logo}
          onClick={() => this.setState({ selectedJob: key })}
        />,
      );
    });

    return (
      <div className="dashboardJobList">
        {selectedJob ? (
          <JobDetail
            onBackButtonClick={() => this.setState({ selectedJob: null })}
            data={jobs[selectedJob]}
            preview
          />
        ) : (
          jobList
        )}
      </div>
    );
  }
}

JobOfferListing.propTypes = {
  jobs: PropTypes.shape({}),
};

export default JobOfferListing;
