import './style.sass';

import {
  Button,
  Card,
  CardActions,
  CardContent,
  Checkbox,
  FormControl,
  FormControlLabel,
  FormGroup,
  InputLabel,
  MenuItem,
  Paper,
  Select,
  Typography,
} from '@material-ui/core';
import React, { PureComponent } from 'react';

import PropTypes from 'prop-types';
import SETTINGS from '../../../config';
import { acceptApplication } from '../../../store/applications/actions';
import { cardHeaderStyle } from '../../../helpers';
import { connect } from 'react-redux';

class ApplicationListing extends PureComponent {
  state = {
    checkedAll: true,
    checkedRejected: false,
    checkedAccepted: false,
    selectedJob: 'all',
  };

  checkboxToValue = {
    checkedAll: null,
    checkedRejected: false,
    checkedAccepted: true,
  };

  handleChange = name => event => {
    this.setState({ [name]: event.target.checked });
  };

  handleSelectChange = event => {
    this.setState({ [event.target.name]: event.target.value });
  };

  acceptedValues = () => {
    const { checkedAll, checkedAccepted, checkedRejected } = this.state;
    const accepted = {};
    if (checkedAccepted) {
      accepted.true = true;
    }
    if (checkedAll) {
      accepted.null = true;
      accepted.true = true;
      accepted.false = true;
    }

    if (checkedRejected) {
      accepted.false = true;
    }
    return accepted;
  };

  filterByStatuses = apps => {
    const acceptedValues = this.acceptedValues();
    const appFilter = Object.keys(apps).filter(key => apps[key].accepted in acceptedValues);
    const { selectedJob } = this.state;
    if (selectedJob !== 'all') {
      return appFilter.filter(key => apps[key].job === selectedJob);
    }
    return appFilter;
  };

  acceptApp = (id, accepted) => {
    const { acceptApplicationAction } = this.props;
    acceptApplicationAction(id, accepted);
  };

  render() {
    const { apps, jobs } = this.props;
    const { checkedRejected, checkedAccepted, checkedAll, selectedJob } = this.state;
    const applicationList = [];
    const jobList = [
      <MenuItem className="menuItem" key="all" value="all">
        All
      </MenuItem>,
    ];
    Object.keys(jobs).forEach(key => {
      jobList.push(
        <MenuItem className="menuItem" key={key} value={jobs[key].title}>
          {' '}
          {jobs[key].title}{' '}
        </MenuItem>,
      );
    });

    this.filterByStatuses(apps).forEach(key => {
      const headerClassName = `appHeader ${cardHeaderStyle(apps[key].accepted)}`;
      applicationList.push(
        <Card className="appCard" key={apps[key].id}>
          <CardContent>
            <h2 className={headerClassName}>{apps[key].job}</h2>
            <Typography gutterBottom variant="headline" component="h4">
              {apps[key].name}
            </Typography>
            {apps[key].comment}
          </CardContent>
          <CardActions>
            {apps[key].cv && (
              <Button className="btn">
                <a
                  className="download"
                  href={`${SETTINGS.API_URLS.BACKEND}${apps[key].cv}`}
                  download>
                  Download CV
                </a>
              </Button>
            )}
            <Button onClick={() => this.acceptApp(key, true)} className="btn">
              Accept{' '}
            </Button>
            <Button onClick={() => this.acceptApp(key, false)} className="btn">
              {' '}
              Reject{' '}
            </Button>
          </CardActions>
        </Card>,
      );
    });

    return (
      <Paper>
        <FormGroup className="appScreenContainer" row>
          <FormControlLabel
            control={
              <Checkbox
                checked={checkedAll}
                onChange={this.handleChange('checkedAll')}
                value="checkedAll"
                color="primary"
              />
            }
            label={<span>All</span>}
          />
          <FormControlLabel
            control={
              <Checkbox
                checked={checkedAccepted}
                onChange={this.handleChange('checkedAccepted')}
                value="checkedAccepted"
                color="primary"
              />
            }
            label={<span>Accepted</span>}
          />
          <FormControlLabel
            control={
              <Checkbox
                checked={checkedRejected}
                onChange={this.handleChange('checkedRejected')}
                value="checkedRejected"
                color="primary"
              />
            }
            label={<span>Rejected</span>}
          />
          <FormControl>
            <InputLabel htmlFor="job-select">
              <span> Jobs...</span>
            </InputLabel>
            <Select
              onChange={this.handleSelectChange}
              className="jobSelect"
              inputProps={{ id: 'job-select', name: 'selectedJob' }}
              value={selectedJob}>
              {jobList}
            </Select>
          </FormControl>
        </FormGroup>
        <div className="cardContainer">{applicationList}</div>
      </Paper>
    );
  }
}

ApplicationListing.propTypes = {
  apps: PropTypes.shape({}),
  jobs: PropTypes.shape({}),
  acceptApplicationAction: PropTypes.func.isRequired,
};

ApplicationListing.defaultProps = {
  apps: {},
  jobs: {},
};

export default connect(
  null,
  { acceptApplicationAction: acceptApplication },
)(ApplicationListing);
