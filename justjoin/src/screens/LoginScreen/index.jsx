import './style.sass';

import * as actions from '../../store/auth/actions';

import React, { PureComponent } from 'react';

import LoginForm from './LoginForm';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

export class LoginScreen extends PureComponent {
  state = {
    email: '',
    password: '',
    errorText: '',
  };

  handleChange = (name, val) => {
    this.setState({ [name]: val });
  };

  handleSubmit = () => {
    const { state } = this;
    const { fetchAuthToken, history } = this.props;
    fetchAuthToken(state).then(token => {
      if (token) {
        history.push('/');
      } else {
        this.setState({ password: '', errorText: 'error' });
      }
    });
  };

  render() {
    const { email, password, errorText } = this.state;
    return (
      <div className="formContainer">
        <LoginForm
          onChange={this.handleChange}
          email={email}
          password={password}
          onSubmit={this.handleSubmit}
        />
        <div className="error">{errorText}</div>
      </div>
    );
  }
}
const mapStateToProps = state => ({ token: state.auth.token });

LoginScreen.propTypes = {
  fetchAuthToken: PropTypes.func.isRequired,
  history: PropTypes.object, // eslint-disable-line react/forbid-prop-types
};

export default connect(
  mapStateToProps,
  {
    fetchAuthToken: actions.fetchAuthToken,
  },
)(LoginScreen);
