import Button from '@material-ui/core/Button';
import FormControl from '@material-ui/core/FormControl';
import PropTypes from 'prop-types';
import React from 'react';
import TextField from '@material-ui/core/TextField';

const LoginForm = ({ onChange, onSubmit, email, password }) => (
  <FormControl className="loginForm">
    <TextField
      value={email}
      placeholder="E-mail"
      onChange={evt => onChange('email', evt.target.value)}
    />
    <TextField
      placeholder="Password"
      type="password"
      value={password}
      onChange={evt => onChange('password', evt.target.value)}
    />
    <Button onClick={onSubmit} type="submit">
      Log in
    </Button>
  </FormControl>
);

LoginForm.propTypes = {
  onChange: PropTypes.func.isRequired,
  onSubmit: PropTypes.func.isRequired,
  email: PropTypes.string.isRequired,
  password: PropTypes.string.isRequired,
};

export default LoginForm;
