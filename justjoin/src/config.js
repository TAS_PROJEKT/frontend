const settings = {
  API_URLS: {
    API: 'http://10.100.6.3:8000/api/',
    BACKEND: 'http://10.100.6.3:8000',
    LOGIN: 'auth/obtain-token/',
    REGISTER: 'auth/register/',
    LOGOUT: 'auth/logout/',
    JOBS: 'main/jobs/',
    TOOLS: 'main/tools',
    USER_DATA: 'auth/user-data',
    COMPANIES: 'company/companies/',
    APPLICATIONS: 'main/applications/',
    COMPANY_JOBS: 'main/company-jobs',
    FILES: 'main/upload/',
    REVIEWS: 'company/reviews/',
  },
  JOB_FILTER_KEYS: {
    exp: 'experience',
    location: 'location',
    tool: 'tool',
    salary: 'salary',
  },
  URLS: {
    LOGIN: '/login',
    REGISTER: '/register',
    ADD_JOB: '/add-job',
    DASHBOARD: '/dashboard',
    ROOT: '/',
    COMPANY: '/company',
    COMPANIES: '/companies',
  },

  EXPERIENCE_LIMIT: 5,
  DEFAULT_EXPERIENCE: 3,
  RATING_LIMIT: 5,

  IMAGE_EXTENSIONS: ['jpg', 'jpeg', 'png'],
  CV_EXTENSIONS: ['pdf'],
};

export default settings;
