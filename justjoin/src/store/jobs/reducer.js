import {
  FETCH_JOBS_FULFILLED,
  FETCH_JOBS_PENDING,
  FETCH_JOBS_REJECTED,
  POST_JOB_FULFILLED,
  POST_JOB_PENDING,
  POST_JOB_REJECTED,
  SET_FILTER,
} from './definitions';

import { arrayWithIdsToObject } from '../../helpers';

const initialState = {
  pending: false,
  error: '',
  data: {},
  filters: {
    exp: '',
    location: '',
    tool: '',
    salary: 0,
  },
};

const jobs = (state = initialState, action) => {
  switch (action.type) {
    case POST_JOB_PENDING:
    case FETCH_JOBS_PENDING:
      return { ...state, pending: true };
    case FETCH_JOBS_FULFILLED: {
      const { data, id, filter } = action.payload;
      const preparedData = arrayWithIdsToObject(data);

      let newFilters = null;
      if (filter) {
        newFilters = { ...state.filters, [filter]: id };
      } else {
        newFilters = { ...state.filters };
      }
      return {
        ...state,
        data: preparedData,
        filters: newFilters,
        pending: false,
      };
    }
    case POST_JOB_FULFILLED:
      return {
        ...state,
        data: { ...state.data, [action.payload.id]: action.payload },
        pending: false,
      };
    case POST_JOB_REJECTED:
    case FETCH_JOBS_REJECTED:
      return { ...state, error: action.payload, pending: false };
    case SET_FILTER:
      return {
        ...state,
        filters: { ...state.filters, [action.payload.filter]: action.payload.id },
      };
    default:
      return state;
  }
};

export default jobs;
