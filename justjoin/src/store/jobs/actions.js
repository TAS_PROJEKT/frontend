import {
  FETCH_JOBS_FULFILLED,
  FETCH_JOBS_PENDING,
  FETCH_JOBS_REJECTED,
  POST_JOB_FULFILLED,
  POST_JOB_PENDING,
  POST_JOB_REJECTED,
  SET_FILTER,
} from './definitions';

import SETTINGS from '../../config';
import axios from 'axios';
import camelCaseKeys from 'camelcase-keys';
import { checkFilters } from '../../helpers';

export const fetchJobs = (filter = null, id = null) => (dispatch, getState) => {
  const state = getState();
  const { token } = state.auth;
  const { filters } = state.jobs;
  const filterKeys = Object.keys(filters);

  let url = `${SETTINGS.API_URLS.API}${SETTINGS.API_URLS.JOBS}`;
  const filtersInState = checkFilters(filters);
  if (filtersInState && filterKeys) {
    url += `?`;
    filterKeys.forEach(key => {
      if (filters[key] && key !== filter) {
        url += `${key}=${filters[key]}&`;
      }
    });
  }

  if (filter && id) {
    dispatch({ type: SET_FILTER, payload: { filter, id } });
    if (url.indexOf('?') > -1) {
      url += `${filter}=${id}`;
    } else {
      url += `?${filter}=${id}`;
    }
  }

  dispatch({ type: FETCH_JOBS_PENDING });

  return axios({
    method: 'GET',
    url,
    headers: { Authorization: `JWT ${token}` },
  })
    .then(res => {
      dispatch({
        type: FETCH_JOBS_FULFILLED,
        payload: { data: camelCaseKeys(res.data), filter, id },
      });
    })
    .catch(err => {
      dispatch({ type: FETCH_JOBS_REJECTED, payload: err.toString() });
    });
};

export const postJob = data => (dispatch, getState) => {
  dispatch({ type: POST_JOB_PENDING });

  const { token } = getState().auth;
  const url = `${SETTINGS.API_URLS.API}${SETTINGS.API_URLS.JOBS}`;

  return axios({
    method: 'POST',
    url,
    headers: { Authorization: `JWT ${token}` },
    data,
  })
    .then(res => {
      dispatch({
        type: POST_JOB_FULFILLED,
        payload: camelCaseKeys(res.data),
      });
    })
    .catch(err => {
      dispatch({ type: POST_JOB_REJECTED, payload: err.toString() });
    });
};

export const fetchCompanyJobs = () => (dispatch, getState) => {
  dispatch({ type: FETCH_JOBS_PENDING });

  const { token } = getState().auth;
  const url = `${SETTINGS.API_URLS.API}${SETTINGS.API_URLS.COMPANY_JOBS}`;

  return axios({
    method: 'GET',
    url,
    headers: { Authorization: `JWT ${token}` },
  })
    .then(res => {
      dispatch({
        type: FETCH_JOBS_FULFILLED,
        payload: { data: camelCaseKeys(res.data) },
      });
    })
    .catch(err => {
      dispatch({ type: FETCH_JOBS_REJECTED, payload: err.toString() });
    });
};
