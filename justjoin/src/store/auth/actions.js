import {
  FETCH_TOKEN_FULFILLED,
  FETCH_TOKEN_PENDING,
  FETCH_TOKEN_REJECTED,
  FETCH_USER_DATA_FULFILLED,
  FETCH_USER_DATA_PENDING,
  FETCH_USER_DATA_REJECTED,
  LOGOUT_FULFILLED,
  LOGOUT_PENDING,
  LOGOUT_REJECTED,
  REGISTER_FULFILLED,
  REGISTER_PENDING,
  REGISTER_REJECTED,
} from './definitions';

import SETTINGS from '../../config';
import axios from 'axios';
import camelCaseKeys from 'camelcase-keys';

export const fetchUserData = () => (dispatch, getState) => {
  const state = getState();
  const { token } = state.auth;

  dispatch({ type: FETCH_USER_DATA_PENDING });

  return axios({
    method: 'GET',
    url: `${SETTINGS.API_URLS.API}${SETTINGS.API_URLS.USER_DATA}`,
    headers: { Authorization: `JWT ${token}` },
  })
    .then(res => {
      dispatch({
        type: FETCH_USER_DATA_FULFILLED,
        payload: camelCaseKeys(res.data, { deep: true }),
      });
      return camelCaseKeys(res.data, { deep: true });
    })
    .catch(err => {
      dispatch({ type: FETCH_USER_DATA_REJECTED, payload: err.toString() });
      return err;
    });
};

export const fetchAuthToken = data => dispatch => {
  dispatch({ type: FETCH_TOKEN_PENDING });
  return axios({
    method: 'POST',
    url: `${SETTINGS.API_URLS.API}${SETTINGS.API_URLS.LOGIN}`,
    data,
  })
    .then(res => {
      dispatch({ type: FETCH_TOKEN_FULFILLED, payload: res.data.token });
      dispatch(fetchUserData());
      return res.data.token;
    })
    .catch(err => {
      dispatch({ type: FETCH_TOKEN_REJECTED, payload: err.toString() });
    });
};

export const register = data => dispatch => {
  dispatch({ type: REGISTER_PENDING });
  return axios({
    method: 'POST',
    url: `${SETTINGS.API_URLS.API}${SETTINGS.API_URLS.REGISTER}`,
    data,
  })
    .then(res => {
      dispatch({ type: REGISTER_FULFILLED });
      return res.status;
    })
    .catch(err => {
      dispatch({ type: REGISTER_REJECTED, payload: err.toString() });
    });
};

export const logout = () => (dispatch, getState) => {
  const { token } = getState().auth;
  dispatch({ type: LOGOUT_PENDING });
  return axios({
    method: 'GET',
    url: `${SETTINGS.API_URLS.API}${SETTINGS.API_URLS.LOGOUT}`,
    headers: { Authorization: `JWT ${token}` },
  })
    .then(() => {
      dispatch({ type: LOGOUT_FULFILLED });
    })
    .catch(err => {
      dispatch({ type: LOGOUT_REJECTED });
      return err;
    });
};
