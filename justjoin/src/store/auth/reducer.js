import * as types from './definitions';

export const initialState = {
  token: localStorage.getItem('token') || '',
  pending: false,
  error: '',
  registered: false,
  data: {
    email: localStorage.getItem('email') || '',
  },
};

const auth = (state = initialState, action) => {
  switch (action.type) {
    case types.LOGOUT_PENDING:
    case types.REGISTER_PENDING:
    case types.FETCH_TOKEN_PENDING:
      return {
        ...state,
        pending: true,
      };
    case types.FETCH_USER_DATA_PENDING:
      return {
        ...state,
        pending: true,
      };
    case types.LOGOUT_REJECTED:
    case types.REGISTER_REJECTED:
    case types.FETCH_TOKEN_REJECTED:
      return {
        ...state,
        error: action.payload,
      };
    case types.FETCH_USER_DATA_REJECTED:
      return {
        ...state,
        error: action.payload,
      };
    case types.FETCH_TOKEN_FULFILLED:
      localStorage.setItem('token', action.payload);
      return {
        ...state,
        token: action.payload,
      };
    case types.REGISTER_FULFILLED:
      return {
        ...state,
        registered: true,
      };
    case types.LOGOUT_FULFILLED: {
      localStorage.removeItem('token');
      localStorage.removeItem('email');
      return {
        ...state,
        token: '',
        data: {},
        pending: false,
      };
    }
    case types.FETCH_USER_DATA_FULFILLED: {
      const preparedData = action.payload;
      localStorage.setItem('email', preparedData.email);
      return { ...state, data: preparedData };
    }
    default:
      return state;
  }
};

export default auth;
