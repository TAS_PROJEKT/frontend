import { applyMiddleware, combineReducers, compose, createStore } from 'redux';
import { persistReducer, persistStore } from 'redux-persist';

import applications from './applications/reducer';
import auth from './auth/reducer';
import companies from './companies/reducer';
import jobs from './jobs/reducer';
import storage from 'redux-persist/lib/storage';
import thunk from 'redux-thunk';
import tools from './tools/reducer';

const middlewares = applyMiddleware(thunk);

const rootReducer = combineReducers({ auth, jobs, tools, companies, applications });

const persistConfig = {
  key: 'root',
  storage,
  whitelist: ['auth', 'applications'],
};

const persistedReducer = persistReducer(persistConfig, rootReducer);

export const store = createStore(
  persistedReducer,
  compose(
    middlewares,
    window.devToolsExtension ? window.devToolsExtension() : f => f,
  ),
);

export const persistor = persistStore(store);
