export const FETCH_APPLICATIONS_FULFILLED = 'applications/FETCH_APPLICATIONS_FULFILLED';

export const FETCH_APPLICATIONS_PENDING = 'applications/FETCH_APPLICATIONS_PENDING';

export const FETCH_APPLICATIONS_REJECTED = 'applications/FETCH_APPLICATIONS_REJECTED';

export const ACCEPT_APPLICATION_FULFILLED = 'applications/ACCEPT_APPLICATION_FULFILLED';

export const ACCEPT_APPLICATION_PENDING = 'applications/ACCEPT_APPLICATION_PENDING';

export const ACCEPT_APPLICATION_REJECTED = 'applications/ACCEPT_APPLICATION_REJECTED';

export const UPLOAD_CV_FULFILLED = 'applications/UPLOAD_CV_FULFILLED';

export const UPLOAD_CV_PENDING = 'applications/UPLOAD_CV_PENDING';

export const UPLOAD_CV_REJECTED = 'applications/UPLOAD_CV_REJECTED';

export const CREATE_APPLICATION_FULFILLED = 'applications/CREATE_APPLICATION_FULFILLED';

export const CREATE_APPLICATION_PENDING = 'applications/CREATE_APPLICATION_PENDING';

export const CREATE_APPLICATION_REJECTED = 'applications/CREATE_APPLICATION_REJECTED';

export const UPDATE_APPLICATION_FULFILLED = 'applications/UPDATE_APPLICATION_FULFILLED';

export const UPDATE_APPLICATION_PENDING = 'applications/UPDATE_APPLICATION_PENDING';

export const UPDATE_APPLICATION_REJECTED = 'applications/UPDATE_APPLICATION_REJECTED';

export const DELETE_APPLICATION_FULFILLED = 'applications/DELETE_APPLICATION_FULFILLED';

export const DELETE_APPLICATION_PENDING = 'applications/DELETE_APPLICATION_PENDING';

export const DELETE_APPLICATION_REJECTED = 'applications/DELETE_APPLICATION_REJECTED';
