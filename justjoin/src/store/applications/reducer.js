import {
  ACCEPT_APPLICATION_FULFILLED,
  ACCEPT_APPLICATION_PENDING,
  ACCEPT_APPLICATION_REJECTED,
  CREATE_APPLICATION_FULFILLED,
  CREATE_APPLICATION_PENDING,
  CREATE_APPLICATION_REJECTED,
  DELETE_APPLICATION_FULFILLED,
  DELETE_APPLICATION_PENDING,
  DELETE_APPLICATION_REJECTED,
  FETCH_APPLICATIONS_FULFILLED,
  FETCH_APPLICATIONS_PENDING,
  FETCH_APPLICATIONS_REJECTED,
  UPDATE_APPLICATION_FULFILLED,
  UPDATE_APPLICATION_PENDING,
  UPDATE_APPLICATION_REJECTED,
  UPLOAD_CV_FULFILLED,
  UPLOAD_CV_PENDING,
  UPLOAD_CV_REJECTED,
} from './definitions';

import { arrayWithIdsToObject } from '../../helpers';

const initialState = {
  pending: false,
  uploaded: false,
  uploading: false,
  error: '',
  data: {},
  currentApplication: {},
  file: '',
};

const applications = (state = initialState, action) => {
  switch (action.type) {
    case DELETE_APPLICATION_PENDING:
    case UPDATE_APPLICATION_PENDING:
    case CREATE_APPLICATION_PENDING:
    case FETCH_APPLICATIONS_PENDING:
    case ACCEPT_APPLICATION_PENDING:
      return { ...state, pending: true };
    case UPLOAD_CV_PENDING:
      return { ...state, uploading: true };
    case FETCH_APPLICATIONS_FULFILLED: {
      const preparedData = arrayWithIdsToObject(action.payload);

      return { ...state, data: { ...state.data, ...preparedData }, pending: false };
    }
    case ACCEPT_APPLICATION_FULFILLED: {
      return {
        ...state,
        pending: false,
        data: { ...state.data, [action.payload.id]: action.payload },
      };
    }
    case CREATE_APPLICATION_FULFILLED:
      return { ...state, currentApplication: action.payload };
    case UPDATE_APPLICATION_FULFILLED:
      return {
        ...state,
        uploaded: false,
        uploading: false,
        currentApplication: {},
        file: '',
        error: '',
      };
    case UPLOAD_CV_FULFILLED:
      return {
        ...state,
        uploaded: true,
        uploading: false,
        pending: false,
        error: '',
        file: action.payload.cv,
      };
    case DELETE_APPLICATION_FULFILLED:
      return { ...state, uploaded: false, uploading: false, currentApplication: {}, error: '' };
    case DELETE_APPLICATION_REJECTED:
    case UPDATE_APPLICATION_REJECTED:
    case CREATE_APPLICATION_REJECTED:
      return { ...state, error: action.payload, pending: false, currentApplication: {} };
    case UPLOAD_CV_REJECTED:
      return { ...state, error: action.payload, uploaded: false, uploading: false };
    case FETCH_APPLICATIONS_REJECTED:
    case ACCEPT_APPLICATION_REJECTED:
      return { ...state, error: action.payload, pending: false };
    default:
      return state;
  }
};

export default applications;
