import {
  ACCEPT_APPLICATION_FULFILLED,
  ACCEPT_APPLICATION_PENDING,
  ACCEPT_APPLICATION_REJECTED,
  CREATE_APPLICATION_FULFILLED,
  CREATE_APPLICATION_PENDING,
  CREATE_APPLICATION_REJECTED,
  DELETE_APPLICATION_FULFILLED,
  DELETE_APPLICATION_PENDING,
  DELETE_APPLICATION_REJECTED,
  FETCH_APPLICATIONS_FULFILLED,
  FETCH_APPLICATIONS_PENDING,
  FETCH_APPLICATIONS_REJECTED,
  UPDATE_APPLICATION_FULFILLED,
  UPDATE_APPLICATION_PENDING,
  UPDATE_APPLICATION_REJECTED,
  UPLOAD_CV_FULFILLED,
  UPLOAD_CV_PENDING,
  UPLOAD_CV_REJECTED,
} from './definitions';

import SETTINGS from '../../config';
import axios from 'axios';
import camelCaseKeys from 'camelcase-keys';

export const createApplication = job => (dispatch, getState) => {
  const state = getState();
  const { token } = state.auth;
  const { currentApplication } = state.applications;
  if (Object.keys(currentApplication).length > 0) {
    return currentApplication;
  }

  dispatch({ type: CREATE_APPLICATION_PENDING });

  return axios({
    method: 'POST',
    url: `${SETTINGS.API_URLS.API}${SETTINGS.API_URLS.APPLICATIONS}`,
    headers: { Authorization: `JWT ${token}` },
    data: { job },
  })
    .then(res => {
      dispatch({ type: CREATE_APPLICATION_FULFILLED, payload: camelCaseKeys(res.data) });
      return res.data;
    })
    .catch(err => {
      dispatch({ type: CREATE_APPLICATION_REJECTED, payload: err.toString() });
      return err.response.status;
    });
};

export const updateApplication = data => (dispatch, getState) => {
  const state = getState();
  const { token } = state.auth;
  const { currentApplication } = state.applications;

  dispatch({ type: UPDATE_APPLICATION_PENDING });

  return axios({
    method: 'PUT',
    url: `${SETTINGS.API_URLS.API}${SETTINGS.API_URLS.APPLICATIONS}${currentApplication.id}/`,
    headers: { Authorization: `JWT ${token}` },
    data,
  })
    .then(res => {
      dispatch({ type: UPDATE_APPLICATION_FULFILLED, payload: camelCaseKeys(res.data) });
    })
    .catch(err => {
      dispatch({ type: UPDATE_APPLICATION_REJECTED, payload: err.toString() });
    });
};

export const deleteApplication = () => (dispatch, getState) => {
  const state = getState();
  const { token } = state.auth;
  const { currentApplication } = state.applications;

  dispatch({ type: DELETE_APPLICATION_PENDING });

  return axios({
    method: 'DELETE',
    url: `${SETTINGS.API_URLS.API}${SETTINGS.API_URLS.APPLICATIONS}${currentApplication.id}/`,
    headers: { Authorization: `JWT ${token}` },
  })
    .then(() => {
      dispatch({ type: DELETE_APPLICATION_FULFILLED });
    })
    .catch(err => {
      dispatch({ type: DELETE_APPLICATION_REJECTED, payload: err.toString() });
    });
};

export const fetchApplications = () => (dispatch, getState) => {
  const state = getState();
  const { token } = state.auth;
  const { company } = state.auth.data;

  if (Object.keys(company).length > 0) {
    dispatch({ type: FETCH_APPLICATIONS_PENDING });

    return axios({
      method: 'GET',
      url: `${SETTINGS.API_URLS.API}${SETTINGS.API_URLS.APPLICATIONS}?company=${company.id}`,
      headers: { Authorization: `JWT ${token}` },
    })
      .then(res => {
        dispatch({ type: FETCH_APPLICATIONS_FULFILLED, payload: camelCaseKeys(res.data) });
      })
      .catch(err => {
        dispatch({ type: FETCH_APPLICATIONS_REJECTED, payload: err.toString() });
      });
  }
};

export const acceptApplication = (id, accepted) => (dispatch, getState) => {
  const state = getState();
  const { token } = state.auth;

  dispatch({ type: ACCEPT_APPLICATION_PENDING });

  return axios({
    method: 'PUT',
    url: `${SETTINGS.API_URLS.API}${SETTINGS.API_URLS.APPLICATIONS}${id}/accept/`,
    headers: { Authorization: `JWT ${token}` },
    data: { accepted },
  })
    .then(res => {
      dispatch({ type: ACCEPT_APPLICATION_FULFILLED, payload: camelCaseKeys(res.data) });
      return camelCaseKeys(res.data);
    })
    .catch(err => {
      dispatch({ type: ACCEPT_APPLICATION_REJECTED, payload: err.toString() });
    });
};

export const uploadCv = (file, id) => (dispatch, getState) => {
  const state = getState();
  const { token } = state.auth;
  dispatch({ type: UPLOAD_CV_PENDING });

  const data = new FormData();
  data.append('cv', file);

  return axios({
    method: 'PUT',
    url: `${SETTINGS.API_URLS.API}${SETTINGS.API_URLS.APPLICATIONS}${id}/cv/`,
    headers: { Authorization: `JWT ${token}`, 'content-type': 'multipart/form-data' },
    data,
  })
    .then(res => {
      dispatch({ type: UPLOAD_CV_FULFILLED, payload: camelCaseKeys(res.data) });
      return camelCaseKeys(res.data);
    })
    .catch(err => {
      dispatch({ type: UPLOAD_CV_REJECTED, payload: err.toString() });
    });
};
