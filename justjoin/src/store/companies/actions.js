import {
  ADD_COMPANY_FULFILLED,
  ADD_COMPANY_PENDING,
  ADD_COMPANY_REJECTED,
  CREATE_REVIEW_FULFILLED,
  CREATE_REVIEW_PENDING,
  CREATE_REVIEW_REJECTED,
  DELETE_REVIEW_FULFILLED,
  DELETE_REVIEW_PENDING,
  DELETE_REVIEW_REJECTED,
  EDIT_COMPANY_FULFILLED,
  EDIT_COMPANY_PENDING,
  EDIT_COMPANY_REJECTED,
  FETCH_COMPANIES_FULFILLED,
  FETCH_COMPANIES_PENDING,
  FETCH_COMPANIES_REJECTED,
  FETCH_COMPANY_FULFILLED,
  FETCH_COMPANY_PENDING,
  FETCH_COMPANY_REJECTED,
  FETCH_COMPANY_REVIEWS_FULFILLED,
  FETCH_COMPANY_REVIEWS_PENDING,
  FETCH_COMPANY_REVIEWS_REJECTED,
  UPDATE_REVIEW_FULFILLED,
  UPDATE_REVIEW_PENDING,
  UPDATE_REVIEW_REJECTED,
  UPLOAD_LOGO_FULFILLED,
  UPLOAD_LOGO_PENDING,
  UPLOAD_LOGO_REJECTED,
} from './definitions';

import SETTINGS from '../../config';
import axios from 'axios';
import camelCaseKeys from 'camelcase-keys';

export const fetchCompanies = () => (dispatch, getState) => {
  const state = getState();
  const { token } = state.auth;

  dispatch({ type: FETCH_COMPANIES_PENDING });

  return axios({
    method: 'GET',
    url: `${SETTINGS.API_URLS.API}${SETTINGS.API_URLS.COMPANIES}`,
    headers: { Authorization: `JWT ${token}` },
  })
    .then(res => {
      dispatch({ type: FETCH_COMPANIES_FULFILLED, payload: camelCaseKeys(res.data) });
    })
    .catch(err => {
      dispatch({ type: FETCH_COMPANIES_REJECTED, payload: err.toString() });
    });
};

export const fetchCompanyDetail = id => (dispatch, getState) => {
  const state = getState();
  const { token } = state.auth;

  dispatch({ type: FETCH_COMPANY_PENDING });

  return axios({
    method: 'GET',
    url: `${SETTINGS.API_URLS.API}${SETTINGS.API_URLS.COMPANIES}${id}/`,
    headers: { Authorization: `JWT ${token}` },
  })
    .then(res => {
      dispatch({ type: FETCH_COMPANY_FULFILLED, payload: camelCaseKeys(res.data) });
    })
    .catch(err => {
      dispatch({ type: FETCH_COMPANY_REJECTED, payload: err.toString() });
    });
};

export const fetchCompanyReviews = id => (dispatch, getState) => {
  const state = getState();
  const { token } = state.auth;

  dispatch({ type: FETCH_COMPANY_REVIEWS_PENDING });

  return axios({
    method: 'GET',
    url: `${SETTINGS.API_URLS.API}${SETTINGS.API_URLS.COMPANIES}${id}/reviews/`,
    headers: { Authorization: `JWT ${token}` },
  })
    .then(res => {
      dispatch({
        type: FETCH_COMPANY_REVIEWS_FULFILLED,
        payload: { data: camelCaseKeys(res.data), id },
      });
    })
    .catch(err => {
      dispatch({ type: FETCH_COMPANY_REVIEWS_REJECTED, payload: err.toString() });
    });
};

export const addCompany = data => (dispatch, getState) => {
  const state = getState();
  const { token } = state.auth;
  dispatch({ type: ADD_COMPANY_PENDING });
  return axios({
    method: 'POST',
    url: `${SETTINGS.API_URLS.API}${SETTINGS.API_URLS.COMPANIES}`,
    headers: { Authorization: `JWT ${token}` },
    data,
  })
    .then(res => {
      dispatch({
        type: ADD_COMPANY_FULFILLED,
        payload: { data: camelCaseKeys(res.data) },
      });
      return camelCaseKeys(res.data);
    })
    .catch(err => {
      dispatch({ type: ADD_COMPANY_REJECTED, payload: err.toString() });
    });
};

export const editCompany = (data, id) => (dispatch, getState) => {
  const state = getState();
  const { token } = state.auth;

  dispatch({ type: EDIT_COMPANY_PENDING });

  return axios({
    method: 'PUT',
    url: `${SETTINGS.API_URLS.API}${SETTINGS.API_URLS.COMPANIES}${id}/`,
    headers: { Authorization: `JWT ${token}` },
    data,
  })
    .then(res => {
      dispatch({
        type: EDIT_COMPANY_FULFILLED,
        payload: { data: camelCaseKeys(res.data) },
      });
    })
    .catch(err => {
      dispatch({ type: EDIT_COMPANY_REJECTED, payload: err.toString() });
    });
};

export const uploadLogo = (file, id) => (dispatch, getState) => {
  const state = getState();
  const { token } = state.auth;
  dispatch({ type: UPLOAD_LOGO_PENDING });

  const data = new FormData();
  data.append('logo', file);

  return axios({
    method: 'PUT',
    url: `${SETTINGS.API_URLS.API}${SETTINGS.API_URLS.COMPANIES}${id}/logo/`,
    headers: { Authorization: `JWT ${token}`, 'content-type': 'multipart/form-data' },
    data,
  })
    .then(res => {
      dispatch({ type: UPLOAD_LOGO_FULFILLED, payload: camelCaseKeys(res.data) });
      return camelCaseKeys(res.data);
    })
    .catch(err => {
      dispatch({ type: UPLOAD_LOGO_REJECTED, payload: err.toString() });
    });
};

export const createReview = company => (dispatch, getState) => {
  const state = getState();
  const { token } = state.auth;
  const { currentReview } = state.companies;
  if (Object.keys(currentReview).length > 0) {
    return currentReview;
  }

  dispatch({ type: CREATE_REVIEW_PENDING });

  return axios({
    method: 'POST',
    url: `${SETTINGS.API_URLS.API}${SETTINGS.API_URLS.REVIEWS}`,
    headers: { Authorization: `JWT ${token}` },
    data: { company },
  })
    .then(res => {
      dispatch({ type: CREATE_REVIEW_FULFILLED, payload: camelCaseKeys(res.data) });
      return res.data;
    })
    .catch(err => {
      dispatch({ type: CREATE_REVIEW_REJECTED, payload: err.toString() });
      return err.response.status;
    });
};

export const updateReview = data => (dispatch, getState) => {
  const state = getState();
  const { token } = state.auth;
  const { currentReview } = state.companies;

  dispatch({ type: UPDATE_REVIEW_PENDING });

  return axios({
    method: 'PUT',
    url: `${SETTINGS.API_URLS.API}${SETTINGS.API_URLS.REVIEWS}${currentReview.id}/`,
    headers: { Authorization: `JWT ${token}` },
    data,
  })
    .then(res => {
      dispatch({
        type: UPDATE_REVIEW_FULFILLED,
        payload: { id: res.data.company, data: camelCaseKeys(res.data) },
      });
    })
    .catch(err => {
      dispatch({ type: UPDATE_REVIEW_REJECTED, payload: err.toString() });
    });
};

export const deleteReview = () => (dispatch, getState) => {
  const state = getState();
  const { token } = state.auth;
  const { currentReview } = state.companies;

  dispatch({ type: DELETE_REVIEW_PENDING });

  return axios({
    method: 'DELETE',
    url: `${SETTINGS.API_URLS.API}${SETTINGS.API_URLS.REVIEWS}${currentReview.id}/`,
    headers: { Authorization: `JWT ${token}` },
  })
    .then(() => {
      dispatch({ type: DELETE_REVIEW_FULFILLED });
    })
    .catch(err => {
      dispatch({ type: DELETE_REVIEW_REJECTED, payload: err.toString() });
    });
};
