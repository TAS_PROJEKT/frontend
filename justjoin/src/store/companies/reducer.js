import {
  CREATE_REVIEW_FULFILLED,
  CREATE_REVIEW_PENDING,
  CREATE_REVIEW_REJECTED,
  DELETE_REVIEW_FULFILLED,
  DELETE_REVIEW_PENDING,
  DELETE_REVIEW_REJECTED,
  FETCH_COMPANIES_FULFILLED,
  FETCH_COMPANIES_PENDING,
  FETCH_COMPANIES_REJECTED,
  FETCH_COMPANY_FULFILLED,
  FETCH_COMPANY_PENDING,
  FETCH_COMPANY_REJECTED,
  FETCH_COMPANY_REVIEWS_FULFILLED,
  FETCH_COMPANY_REVIEWS_PENDING,
  FETCH_COMPANY_REVIEWS_REJECTED,
  UPDATE_REVIEW_FULFILLED,
  UPDATE_REVIEW_PENDING,
  UPDATE_REVIEW_REJECTED,
  UPLOAD_LOGO_FULFILLED,
  UPLOAD_LOGO_PENDING,
  UPLOAD_LOGO_REJECTED,
} from './definitions';

import { arrayWithIdsToObject } from '../../helpers';

const initialState = {
  pending: false,
  error: '',
  uploaded: false,
  uploading: false,
  fileName: '',
  data: {},
  currentReview: {},
};

const companies = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_COMPANIES_PENDING:
    case FETCH_COMPANY_PENDING:
    case FETCH_COMPANY_REVIEWS_PENDING:
    case CREATE_REVIEW_PENDING:
    case UPDATE_REVIEW_PENDING:
    case DELETE_REVIEW_PENDING:
      return { ...state, pending: true };

    case UPLOAD_LOGO_PENDING:
      return { ...state, uploading: true };

    case FETCH_COMPANIES_FULFILLED: {
      const preparedData = arrayWithIdsToObject(action.payload);

      return { ...state, data: { ...state.data, ...preparedData }, pending: false };
    }

    case FETCH_COMPANY_FULFILLED: {
      const { id } = action.payload;

      return { ...state, data: { ...state.data, [id]: action.payload }, pending: false };
    }

    case FETCH_COMPANY_REVIEWS_FULFILLED: {
      const { id } = action.payload;
      return {
        ...state,
        data: {
          ...state.data,
          [id]: { ...state.data[id], reviews: action.payload.data },
        },
        pending: false,
      };
    }

    case UPLOAD_LOGO_FULFILLED:
      return { ...state, uploaded: true, uploading: false, fileName: action.payload.logo };

    case UPLOAD_LOGO_REJECTED:
      return { ...state, uploaded: false, uploading: false, error: action.payload };

    case CREATE_REVIEW_FULFILLED:
      return { ...state, currentReview: action.payload };

    case UPDATE_REVIEW_REJECTED:
    case CREATE_REVIEW_REJECTED:
      return { ...state, pending: false, error: action.payload, currentReview: {} };

    case DELETE_REVIEW_FULFILLED:
    case UPDATE_REVIEW_FULFILLED: {
      const { id, data } = action.payload;
      return {
        ...state,
        pending: false,
        currentReview: {},
        data: {
          ...state.data,
          [id]: { ...state.data[id], companyReview: [...state.data[id].companyReview, data] },
        },
      };
    }

    case FETCH_COMPANIES_REJECTED:
    case FETCH_COMPANY_REJECTED:
    case FETCH_COMPANY_REVIEWS_REJECTED:
    case DELETE_REVIEW_REJECTED:
      return { ...state, error: action.payload };

    default:
      return state;
  }
};

export default companies;
