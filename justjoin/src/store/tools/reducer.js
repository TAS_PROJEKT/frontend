import { FETCH_TOOLS_FULFILLED, FETCH_TOOLS_PENDING, FETCH_TOOLS_REJECTED } from './definitions';

import { arrayWithIdsToObject } from '../../helpers';

const initialState = {
  pending: false,
  error: '',
  data: {},
};

const tools = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_TOOLS_PENDING:
      return { ...state, pending: true };
    case FETCH_TOOLS_FULFILLED: {
      const preparedData = arrayWithIdsToObject(action.payload);

      return { ...state, data: { ...state.data, ...preparedData }, pending: false };
    }
    case FETCH_TOOLS_REJECTED:
      return { ...state, error: action.payload };
    default:
      return state;
  }
};

export default tools;
