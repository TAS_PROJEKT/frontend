import { FETCH_TOOLS_FULFILLED, FETCH_TOOLS_PENDING, FETCH_TOOLS_REJECTED } from './definitions';

import SETTINGS from '../../config';
import axios from 'axios';
import camelCaseKeys from 'camelcase-keys';

const fetchTools = () => (dispatch, getState) => {
  const state = getState();
  const { token } = state.auth;

  dispatch({ type: FETCH_TOOLS_PENDING });

  return axios({
    method: 'GET',
    url: `${SETTINGS.API_URLS.API}${SETTINGS.API_URLS.TOOLS}`,
    headers: { Authorization: `JWT ${token}` },
  })
    .then(res => {
      dispatch({ type: FETCH_TOOLS_FULFILLED, payload: camelCaseKeys(res.data) });
    })
    .catch(err => {
      dispatch({ type: FETCH_TOOLS_REJECTED, payload: err.toString() });
    });
};

export default fetchTools;
