export const arrayWithIdsToObject = data => {
  const preparedData = {};
  if (data) {
    data.forEach(stat => {
      preparedData[stat.id] = stat;
    });
  }

  return preparedData;
};

export const removeKey = (data, deleteKey) => {
  const keys = Object.keys(data).filter(key => key !== deleteKey);
  const newObj = {};

  keys.forEach(key => {
    newObj[key] = data[key];
  });
  return newObj;
};

export const formatToUrl = name => name.toLowerCase().replace(/\s/g, '');

export const experienceToLevelLabel = exp => {
  switch (exp) {
    case 1:
    case 2:
      return 'beginner';
    case 3:
    case 4:
      return 'intermediate';
    case 5:
      return 'advanced';
    default:
      return 'intermediate';
  }
};

export const jobDateToLabel = date => {
  const oldDate = new Date(date);
  const currentDate = new Date();
  const dateDifferenceInDays = (currentDate - oldDate) / 1000 / 60 / 60 / 24;

  if (dateDifferenceInDays <= 1) {
    return 'Today';
  }
  if (dateDifferenceInDays <= 2) {
    return 'Yesterday';
  }

  return `${Math.round(dateDifferenceInDays)} days ago`;
};

export const checkFilters = filters => {
  let exist = false;
  // eslint-disable-next-line
  Object.keys(filters).map(key => {
    if (filters[key]) {
      exist = true;
    }
  });
  return exist;
};

export const cardHeaderStyle = accepted => {
  switch (accepted) {
    case true:
      return 'accepted';
    case false:
      return 'rejected';
    default:
      return '';
  }
};
