import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
} from '@material-ui/core';

import PropTypes from 'prop-types';
import React from 'react';

const PermissionDialog = ({ onClose, text, open }) => (
  <Dialog
    aria-labelledby="perm-dialog-title"
    aria-describedby="perm-dialog-description"
    open={open}>
    <DialogTitle id="perm-dialog-title">
      <span>Permission Denied</span>
    </DialogTitle>
    <DialogContent>
      <DialogContentText id="perm--dialog-desc">
        <span>{text}</span>
      </DialogContentText>
    </DialogContent>
    <DialogActions>
      <Button onClick={onClose} color="primary">
        Ok
      </Button>
    </DialogActions>
  </Dialog>
);

PermissionDialog.propTypes = {
  text: PropTypes.string.isRequired,
  onClose: PropTypes.func.isRequired,
  open: PropTypes.bool,
};

PermissionDialog.defaultProps = {
  open: false,
};

export default PermissionDialog;
