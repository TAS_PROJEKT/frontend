import './style.sass';

import { Button, CircularProgress, Icon } from '@material-ui/core';

import PropTypes from 'prop-types';
import React from 'react';

class FileLoader extends React.PureComponent {
  state = {
    extensionError: '',
  };

  handleChange = evt => {
    const { onFileUpload } = this.props;
    const { files } = evt.target;
    const file = files[0];

    if (file && this.checkFileFormat(file.name)) {
      onFileUpload(file);
      this.setState({ extensionError: '' });
    } else {
      const { extensions } = this.props;
      const exts = extensions.join(', ');
      this.setState({
        extensionError: `Unallowed file extension. Allowed extensions are: ${exts}. `,
      });
    }
  };

  checkFileFormat = fileName => {
    const { extensions } = this.props;
    const fileExtension = fileName.substr(fileName.lastIndexOf('.') + 1);

    if (extensions.includes(fileExtension)) {
      return true;
    }
    return false;
  };

  render() {
    const { label, uploading, uploaded, fileName } = this.props;
    const { extensionError } = this.state;

    return (
      <div className="fileLoader">
        <input onChange={this.handleChange} className="hide" id="button-file" type="file" />
        <span>
          <label className="uploadLabel" htmlFor="button-file">
            <Button component="span">
              {label}
              <Icon>attach_file</Icon>
            </Button>
          </label>
          {uploading && <CircularProgress size={20} />}
          {uploaded && (
            <span className="centered">
              {fileName.substr(13)}{' '}
              <Icon className={extensionError ? 'errorIcon' : 'doneIcon'}>
                {extensionError ? 'error' : 'done'}
              </Icon>
            </span>
          )}
          {extensionError && <div className="error">{extensionError}</div>}
        </span>
      </div>
    );
  }
}

FileLoader.propTypes = {
  label: PropTypes.string,
  onFileUpload: PropTypes.func.isRequired,
  uploading: PropTypes.bool.isRequired,
  uploaded: PropTypes.bool.isRequired,
  fileName: PropTypes.string.isRequired,
  extensions: PropTypes.arrayOf(PropTypes.string),
};

FileLoader.defaultProps = {
  label: 'Upload',
  extensions: ['*'],
};

export default FileLoader;
