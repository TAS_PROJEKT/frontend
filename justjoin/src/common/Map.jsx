import { Map, Marker, TileLayer } from 'react-leaflet';
import React, { PureComponent } from 'react';

import { Button } from '@material-ui/core';
import PropTypes from 'prop-types';
import uuidv4 from 'uuid/v4';

class CustomMap extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      selectedMarker: [],
      drawing: false,
    };
  }

  handleMapClick = e => {
    const { drawing } = this.state;
    const { onMapClick } = this.props;

    if (drawing) {
      const { latlng } = e;
      this.setState({ selectedMarker: [latlng.lat, latlng.lng] }, () => {
        onMapClick({ lat: latlng.lat.toPrecision(6), lng: latlng.lng.toPrecision(6) });
      });
    }
  };

  render() {
    const {
      width,
      height,
      interactive,
      markers,
      buttonLabel,
      onMarkerClick,
      selected,
    } = this.props;
    const { selectedMarker } = this.state;
    let zoom = 4;

    const markerList = [];
    if (interactive && selectedMarker.length === 2) {
      markerList.push(<Marker key={uuidv4()} position={selectedMarker} />);
    } else {
      markers.forEach(marker => {
        markerList.push(
          <Marker
            onClick={() => onMarkerClick(marker.id)}
            key={uuidv4()}
            position={[marker.lat, marker.lng]}
          />,
        );
      });
    }

    let { position } = this.props;
    if (markerList.length === 1) {
      if (selectedMarker.length > 0) {
        position = [selectedMarker[0], selectedMarker[1]];
      } else {
        position = [markers[0].lat, markers[0].lng];
      }
      zoom = 12;
    }

    if (selected) {
      position = selected;
      zoom = 12;
    }

    return (
      <div>
        <Map onClick={this.handleMapClick} style={{ width, height }} center={position} zoom={zoom}>
          <TileLayer
            url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
            attribution="&copy; <a href=&quot;http://osm.org/copyright&quot;>OpenStreetMap</a> contributors"
          />
          {markerList}
        </Map>
        {interactive && (
          <Button className="btn" onClick={() => this.setState({ drawing: true })}>
            {buttonLabel}
          </Button>
        )}
      </div>
    );
  }
}

CustomMap.propTypes = {
  width: PropTypes.number.isRequired,
  height: PropTypes.number.isRequired,
  position: PropTypes.arrayOf(PropTypes.number),
  selected: PropTypes.arrayOf(PropTypes.number),
  markers: PropTypes.arrayOf(
    PropTypes.shape({ id: PropTypes.string, lat: PropTypes.number, lng: PropTypes.number }),
  ),
  interactive: PropTypes.bool,
  onMapClick: PropTypes.func,
  buttonLabel: PropTypes.string,
  onMarkerClick: PropTypes.func,
};

CustomMap.defaultProps = {
  position: [52.467151, 16.927503],
  interactive: false,
  markers: [],
  buttonLabel: 'Add a location',
  onMapClick: () => {},
  onMarkerClick: () => {},
};

export default CustomMap;
