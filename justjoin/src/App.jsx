import { Redirect, Route, BrowserRouter as Router, Switch } from 'react-router-dom';
import { persistor, store } from './store';

import AddJobScreen from './screens/AddJobScreen';
import CompaniesScreen from './screens/CompaniesScreen';
import CompanyDetail from './screens/CompaniesScreen/CompanyDetail';
import Dashboard from './screens/Dashboard';
import Header from './screens/Header';
import JobListingScreen from './screens/JobListingScreen';
import LoginRequired from './HOC/LoginRequired';
import LoginScreen from './screens/LoginScreen';
import { PersistGate } from 'redux-persist/integration/react';
import { Provider } from 'react-redux';
import React from 'react';
import RegisterScreen from './screens/RegisterScreen';

const App = () => (
  <Provider store={store}>
    <PersistGate loading={null} persistor={persistor}>
      <Router>
        <React.Fragment>
          <Header />
          <Switch>
            <Route exact path="/" render={props => <Redirect to="/all" {...props} />} />
            <Route exact path="/login" component={LoginScreen} />
            <Route exact path="/register" component={RegisterScreen} />
            <Route exact path="/dashboard" component={LoginRequired(Dashboard)} />
            <Route exact path="/add-job" component={LoginRequired(AddJobScreen)} />
            <Route exact path="/companies" component={LoginRequired(CompaniesScreen)} />
            <Route exact path="/:tool?:exp?" component={LoginRequired(JobListingScreen)} />
            <Route exact path="/company/:id?" component={LoginRequired(CompanyDetail)} />
          </Switch>
        </React.Fragment>
      </Router>
    </PersistGate>
  </Provider>
);

export default App;
