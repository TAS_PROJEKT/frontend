import PropTypes from 'prop-types';
import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';

export default function LoginRequired(ProtectedComponent) {
  class AuthRoute extends React.PureComponent {
    componentDidMount() {
      const { token } = this.props;
      if (!token) {
        this.redirectToLogin();
      }
    }

    redirectToLogin = () => {
      const { history } = this.props;
      history.push('/login');
    };

    render() {
      const { token } = this.props;

      return <div>{token.length > 0 && <ProtectedComponent {...this.props} />}</div>;
    }
  }

  AuthRoute.propTypes = {
    token: PropTypes.string.isRequired,
    history: PropTypes.shape({}).isRequired,
  };

  const mapStateToProps = state => ({ token: state.auth.token });

  return withRouter(connect(mapStateToProps)(AuthRoute));
}
